<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Wells International | Home</title>
        <meta name="author" content="Wells" />
        <meta name="description" content="wells dev" />
        <meta name="keywords" content="wells" />
        <meta name="Resource-type" content="Document" />
        <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
        <link href="{{asset('css/fe-app.css')}}" rel="stylesheet">

    </head>

    <body>
        <!--<div class="se-pre-con"></div>-->
        <section class="main_menu" id="mainmenu">
            <div class="navMain">
                <nav class="navbar navbar-expand-lg">
                    <div class="top_menu">
                        <ul>
                            <li><a href="#"><img src="images/icons/instagram.svg" alt="Instagram"></a></li>
                            <li><a href="#"><img src="images/icons/pinterest.svg" alt="Pintrest"></a></li>
                            <li>
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> en </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                        <button class="dropdown-item" type="button">ar</button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <svg width="22" height="24" viewBox="0 0 12 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="12" height="0.4" fill="#fff"/>
                            <rect y="4" width="12" height="0.4" fill="#fff"/>
                            <rect y="8" width="12" height="0.4" fill="#fff"/>
                        </svg>
                    </button>
                    <div class="mobile-phone">
                        <a href="tel:+971508868840"><svg width="18" height="28" viewBox="0 0 23 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.7424 30.5299C7.18206 25.0101 2.18994 16.3246 0.201203 8.73499C-1.21932 3.37759 5.31509 -2.06099 6.24858 0.780062L8.60259 8.1262C8.76493 8.73499 8.27789 9.34379 7.75027 9.66848C6.28916 10.4802 5.63978 10.0743 5.15274 10.3584C3.32636 11.4137 12.0118 26.5118 13.8382 25.416C14.3253 25.1319 14.3253 24.3608 15.7458 23.549C16.2734 23.2243 17.0446 23.1026 17.491 23.549L22.6861 29.2717C24.6748 31.504 16.7199 34.4668 12.7424 30.5299Z" fill="white"/>
                            </svg></a>
                    </div>
                    <div class="mobile-phone">
                        <div class="dropdown" style="top: 7px;">
                            <button class="btn dropdown-toggle" style="color:white;" type="button" id="dropdownMenu2"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> en </button>
                            <div class="dropdown-menu" style="padding-top: 20px;background-color:#fff0; border:0px solid rgba(0,0,0,.15);margin:-13px;" aria-labelledby="dropdownMenu2">
                                <button class="dropdown-item" style="color:white; font-size:14px" type="button">ar</button>
                            </div>
                        </div>
                    </div>
                    <div class="nav_flex_2"> 
                        <a class="navbar-brand" href="http://wellsinternational.go-demo.com/">
                            <img src="images/logo-nav.png" width="116" height="59" alt="" />
                        </a> 
                    </div>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <div class="nav_flex_1">
                            <ul class="navbar-nav">
                                <li class="nav-item active"> <a class="nav-link" href="hotels.html">HOTELS</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="fnb.html">F & B</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="spa.html">SPA</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="collections.html">COLLECTIONS</a> </li>
                            </ul>
                        </div>
                        <div class="nav_flex_3">
                            <ul class="navbar-nav float-right">
                                <li class="nav-item active"> <a class="nav-link" href="approach.html">APPROACH</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="manifesto.html">MANIFESTO</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="projects.html">PROJECTS</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="contactus.html">CONTACT</a> </li>
                            </ul>
                        </div>
                        <div class="socials" style="display: none;">
                            <div class="nav_flex_1" style="padding-right:4px;">
                                <a href=""><img src="images/icons/instagram.svg" alt="Instagram"></a>
                            </div>
                            <div class="nav_flex_3" style="padding-left:4px;">
                                <a href="#"><img src="images/icons/pinterest.svg" alt="Pintrest"></a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </section>
        <div id="fullpage">
            <div class="section " id="section0">
                <video id="myVideo" onplay="myFunction()" data-autoplay autoplay muted loop playsinline controls>
                    <source src="images/slides/home.mp4" type="video/mp4">
                    <source src="images/slides/home.webm" type="video/webm">
                </video>
                <div class="layer">
                    <ul class="sub-menu" id="dropDownMenu">
                        <li id="homeid1"><img src="images/logo.svg" width="596" height="130" alt="" class="home_text1" /></li>
                        <li id="homeid2"><img src="images/logo-2.svg" width="467" height="11" alt="" class="home_text2" /></li>
                        <li id="homeid3"><h1 class="slide_two font-cg fadeIn">Global Hospitality Design</h1></li>
                        <li id="homeid4"><h1 class="fontlarge slide_two font-cg fadeIn">Hotels</h1></li>
                        <li id="homeid5"><h1 class="fontlarge slide_two font-cg  fadeIn">F&B</h1></li>
                        <li id="homeid6"><h1 class="fontlarge slide_two font-cg  fadeIn">SPA</h1></li>
                        <li id="homeid7"><h1 class="fontmedium slide_two font-cg fadeIn">Collections</h1></li>
                        <li id="homeid8"><h1 class="stories slide_two font-cg fadeIn">We Design Stories</h1></li>
                    </ul>
                </div>
                <button onclick="enableMute()" type="button" class="mute_btn">
                    <img src="images/icons/mute.png" alt=""/>
                </button>
            </div>
        </div>
        
        <script src="{{asset('js/fe-app.js')}}"></script> 
        <script type="text/javascript">
            $(document).ready(function () {
                $('#fullpage').fullpage({
                    verticalCentered: true,

                    sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE']
                });
            });
        </script> 
        <script>
            function myFunction() {
                var x = document.getElementById("myVideo").autoplay;
            }

            var vid = document.getElementById("myVideo");

            // function enableMute() {
            //     vid.muted = false;
            // }
            
            function enableMute() {
                var video=document.getElementById("myVideo");
            
                video.muted = !video.muted;
            
            }

            jQuery(document).ready(function ($) {

                document.getElementById("myVideo").controls = false;

                // Get the video element with id="myVideo"
                var vid = document.getElementById("myVideo");

                // Assign an ontimeupdate event to the video element, and execute a function if the current playback position has changed
                vid.ontimeupdate = function () {
                    myFunction()
                };

                function myFunction() {
                    // Display the current position of the video in a p element with id="demo"
                    var hello = Math.round(vid.currentTime);

                    if (Math.round(vid.currentTime) == 2) {
                        $('#homeid1').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 4) {
                        $('#homeid2').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 9) {
                        $('#homeid1').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 9) {
                        $('#homeid2').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 14) {
                        $('#homeid3').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 18) {
                        $('#homeid3').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 20) {
                        $('#homeid4').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 22) {
                        $('#homeid4').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 23) {
                        $('#homeid5').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 25) {
                        $('#homeid5').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 26) {
                        $('#homeid6').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 28) {
                        $('#homeid6').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 29) {
                        $('#homeid7').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 31) {
                        $('#homeid7').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 33) {
                        $('#homeid8').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 44) {
                        $('#homeid8').fadeOut(1000);
                    }
                }
            });
        </script>
    </body>
</html>