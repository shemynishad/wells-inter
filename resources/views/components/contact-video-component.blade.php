<video id="myVideo" class="mobile_video" data-autoplay autoplay loop muted playsinline>
    <source src="{{ asset('videos') }}/{{ $content($id)->video_1 }}" type="video/mp4">
    <source src="{{ asset('videos') }}/{{ $content($id)->video_2 }}" type="video/webm">
</video>
<div id="myVideo" class="desktop_video">{!! $content($id)->embed_video !!}</div>
<div class="layer hotels-innner contact_us01" style="padding-bottom:170px; z-index: 9999;">
    @if($content($id)->page_id == 18)
    <div class="contact_usbtn btnmobile" data-aos="fade-up" data-aos-delay="200"><a href="mailto:{{ $content($id)->input_1 }}">
            <font style="font-size:18px !important;">استفسارات الأعمال</font>
        </a></div>
    <div class="contact_usbtn btnmobilegeneral" data-aos="fade-up" data-aos-delay="200"><a href="mailto:{{ $content($id)->input_2 }}">
            <font style="padding-right: 5px; padding-left: 5px; font-size:18px !important;">استفسارات عامة</font>
        </a></div>
    <div class="contact_usbtn btnmobilegeneral" data-aos="fade-up" data-aos-delay="200"><a href="mailto:{{ $content($id)->input_3 }}">
            <font style="padding-right: 5px; padding-left: 5px; font-size:18px !important;">وظائف</font>
        </a></div>
    @else
    <div class="contact_usbtn btnmobile" data-aos="fade-up" data-aos-delay="200"><a href="mailto:{{ $content($id)->input_1 }}">
            <font style="font-size:18px !important;">Business Enquiries</font>
        </a></div>
    <div class="contact_usbtn btnmobilegeneral" data-aos="fade-up" data-aos-delay="200"><a href="mailto:{{ $content($id)->input_2 }}">
            <font style="padding-right: 5px; padding-left: 5px; font-size:18px !important;">General Enquiries</font>
        </a></div>
    <div class="contact_usbtn btnmobilegeneral" data-aos="fade-up" data-aos-delay="200"><a href="mailto:{{ $content($id)->input_3 }}">
            <font style="padding-right: 5px; padding-left: 5px; font-size:18px !important;">Careers</font>
        </a></div>
    @endif
</div>