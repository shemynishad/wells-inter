<div id="fullpage">
    <div class="section active" id="sectionmenifesto">
        <video id="myVideo" class="mobile_video" onplay="myFunction()" data-autoplay autoplay muted playsinline>
            <source src="{{ asset('videos') }}/{{ $content($id)->video_1 }}" type="video/mp4">
            <source src="{{ asset('videos') }}/{{ $content($id)->video_2 }}" type="video/webm">
        </video>
        <div id="myVideo" class="desktop_video">{!! $content($id)->embed_video !!}</div>
        <div class="layer hotels-innner">
            <h1 class="slide_two font-cg hotels" style="display:none" id="approach011">{{ $content($id)->input_1 }}</h1>
            <h1 class="slide_two  font-cg hotels_small" style="display:none" id="approach023">{{ $content($id)->input_2 }}</h1>
        </div>
    </div>

    <div style="display:none" class="section" id="sectionmenifesto2">
        @foreach($content($id)->manifesto_slides as $manifesto_slide)
            @if($loop->iteration == 1)
                <div class="slide active" id="sectionmenifesto{{ $loop->iteration + 2 }}">
            @else
                <div class="slide" id="sectionmenifesto{{ $loop->iteration + 2 }}">
            @endif
                    <div class="manifesto_inner_box">
                        <div class="intro manifesto_intro">
                            <span>{{ $manifesto_slide->sub_heading }}</span>
                            <h2>{{ $manifesto_slide->headline }}</h2>
                            <h3>“{{ $manifesto_slide->quote }}”</h3>
                            <p> {{ $manifesto_slide->text }} </p>
                            <span class="number_mani">{{ $loop->iteration }}</span>
                        </div>
                    </div>
                </div>
        @endforeach
    </div>
    
</div>
<div class="slide-modal" id="sectionmenifesto7">
        <div class="manifesto_inner_box mani_last">
            <div class="intro manifesto_intro manifesto_custom_img">
                <div class="manifesto_intro_inner">
                    <span class="custom_font">{{ $content($id)->headline_1 }}</span>
                    <h2 class="custom_font">{{ $content($id)->headline_2 }}</h2>
                    <h3>“{{ $content($id)->input_3 }}”</h3>
                    {!! html_entity_decode($content($id)->text_1) !!}
                </div>
            </div>            
            <h5 class="manifesto_name close"><a href="#sectionmenifesto3"><img src="{{ asset('images') }}/iconex.png" alt="Close" width="25" height="25"></a></h5>
        </div>
    </div>
<div style="position: relative; z-index: 1;" class="buttondiv">
    <a href="#sectionmenifesto7">
        <h5 class="manifesto_name justinbutton" style="display: none; color:#fff width: 100px;height: 50px;position: absolute;top: 50%;left: 49%;margin: -25px 0 0 -25px;">JUSTIN WELLS</h5>
    </a>
</div>