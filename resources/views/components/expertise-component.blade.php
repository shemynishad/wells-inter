<section data-aos="fade-up" class="section" id="expertise_section">
    <div class="container">
        <div class="col-12">
            <div class="heading_group">
                <span>{{ $content($id)->headline_1 }}</span>
                <h2>{{ $content($id)->headline_2 }}</h2>
            </div>
            <div class="row no-gutters">
                <div data-aos="fade-up" class="col-12 col-md-6 col-lg-3 expertise_box">
                    <img src="{{ asset('images') }}/{{ $content($id)->image_1 }}"  class="img-fluid" alt=""/>
                    <!--<h3>Hotels</h3>-->
                    <h4>{{ $content($id)->headline_3 }}</h4>
                    <p>{{ $content($id)->text_1 }}</p>
                </div>
                <div data-aos="fade-up" class="col-12 col-md-6 col-lg-3 expertise_box">
                    <img src="{{ asset('images') }}/{{ $content($id)->image_2 }}" class="img-fluid" alt=""/>
                    <!--<h3>F&B</h3>-->
                    <h4>{{ $content($id)->headline_4 }}</h4>
                    <p>{{ $content($id)->text_2 }}</p>
                </div>
                <div data-aos="fade-up" class="col-12 col-md-6 col-lg-3 expertise_box">
                    <img src="{{ asset('images') }}/{{ $content($id)->image_3 }}" class="img-fluid" alt=""/>
                    <!--<h3>Spa</h3>-->
                    <h4>{{ $content($id)->headline_5 }}</h4>
                    <p>{{ $content($id)->text_3 }}</p>
                </div>
                <div data-aos="fade-up" class="col-12 col-md-6 col-lg-3 expertise_box">
                    <img src="{{ asset('images') }}/{{ $content($id)->image_4 }}" class="img-fluid" alt=""/>
                    <!--<h3>Collections</h3>-->
                    <h4 class="services-h3">{{ $content($id)->headline_6 }}</h4>
                    <p>{{ $content($id)->text_4 }}</p>
                </div>
            </div>
        </div>
    </div>
</section>