<div class="section" id="sectionhotels03">
	<div class="layer hotel03 hotel_page">
		<h5  data-aos="fade-up" data-aos-delay="100">{{ $content($id)->headline_1 }}</h5>
		<h1 class="slide_two" data-aos="fade-up" data-aos-delay="200">{{ $content($id)->headline_2 }}</h1>
		<h4 data-aos="fade-up" data-aos-delay="400">“{{ $content($id)->headline_3 }}”</h4>
		<h6><span data-aos="fade-up"  data-aos-delay="600">{{ $content($id)->text_1 }}</span></h6>
		<div class="imageslider">
			<div class="">
				<div class="">
					<!-- Create your own class for the containing div -->
					<div class="slick-carousel">
						@foreach($content($id)->carousals as $carousal)
							<!-- Inside the containing div, add one div for each slide -->
							<div class="slid_inner">
								<!-- You can put an image or text inside each slide div -->
								<div class="slick_image"> 
									<img src="{{ asset('images') }}/carousal/{{ $carousal->thumbnail }}" width="100%" alt="">
								</div>
								<div class="content">
									<h3>{{ $carousal->title }}</h3>
									<p class="content-clipped">{{ $carousal->description }}</p>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
</div>