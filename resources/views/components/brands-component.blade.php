<section class="section" id="work-with">
    <div class="container">
        <div class="heading_group white">
            <span>{{ $content($id)->headline_1 }}</span>
            <h2>{{ $content($id)->headline_2 }}</h2>
        </div>
        <div class="li-box">
            <ul>
                @foreach($content($id)->brands as $brand)
                    <li><img data-aos="fade-up" src="{{ asset('images') }}/brands/{{ $brand->brand_image }}" alt="{{ $brand->brand_name }}"></li>
                @endforeach
            </ul>
        </div>
    </div>
</section>