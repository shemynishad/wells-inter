<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <!-- <img src="{{ asset('images/users') }}/{{ Auth::user()->profile_photo_path }}" class="img-circle elevation-2" alt="User Image"> -->
            </div>
            <div class="info">
                <a href="{{ url('/user/profile') }}" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                <!-- Dashboard -->
                <li class="nav-item">
                    <a href="{{ url('/dashboard') }}" class="nav-link {{ 'dashboard' === request()->path() ? 'active' : '' }}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <!-- Logout -->
                <li class="nav-item">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <a href="{{ route('logout') }}" class="nav-link"
                                            onclick="event.preventDefault();
                                                        this.closest('form').submit();">
                            <i class="nav-icon fas fa-power-off"></i>
                            <p>
                                {{ __('Logout') }}
                            </p>
                        </a>
                    </form>
                </li>
                <!-- Users Management -->
                <li class="nav-item {{ (request()->is('user/*') || request()->is('users/*') || request()->is('users')) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ (request()->is('users/*') || request()->is('users') || request()->is('user/profile')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tag"></i>
                        <p>
                            User Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview ">
                        <li class="nav-item">
                            <a href="{{ url('user/profile') }}" class="nav-link {{ 'user/profile' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Profile</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('users') }}" class="nav-link {{ 'users' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/users/create') }}" class="nav-link {{ 'users/create' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- Pages Management -->
                <li class="nav-item {{ (request()->is('admin/pages/*')) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ (request()->is('admin/pages/*') || request()->is('admin/pages')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tag"></i>
                        <p>
                            Page Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview ">
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/1') }}" class="nav-link {{ 'admin/pages/1' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Home</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/2') }}" class="nav-link {{ 'admin/pages/2' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Hotels</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/3') }}" class="nav-link {{ 'admin/pages/3' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>F & B</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/4') }}" class="nav-link {{ 'admin/pages/4' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>SPA</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/5') }}" class="nav-link {{ 'admin/pages/5' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Collections</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/6') }}" class="nav-link {{ 'admin/pages/6' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Approach</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/7') }}" class="nav-link {{ 'admin/pages/7' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manifesto</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/8') }}" class="nav-link {{ 'admin/pages/8' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Projects</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/9') }}" class="nav-link {{ 'admin/pages/9' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Contact us</p>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="{{ url('/admin/pages/10') }}" class="nav-link {{ 'admin/pages/10' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Home (ar)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/11') }}" class="nav-link {{ 'admin/pages/11' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Hotels (ar)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/12') }}" class="nav-link {{ 'admin/pages/12' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>F & B (ar)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/13') }}" class="nav-link {{ 'admin/pages/13' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>SPA (ar)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/14') }}" class="nav-link {{ 'admin/pages/14' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Collections (ar)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/15') }}" class="nav-link {{ 'admin/pages/15' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Approach (ar)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/16') }}" class="nav-link {{ 'admin/pages/16' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manifesto (ar)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/17') }}" class="nav-link {{ 'admin/pages/17' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Projects (ar)</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/admin/pages/18') }}" class="nav-link {{ 'admin/pages/18' === request()->path() ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Contact us (ar)</p>
                            </a>
                        </li>
                    </ul>
                </li> -->
                <li class="nav-item">
                    <a href="{{ url('/admin/social-media/1/edit') }}" class="nav-link {{ 'admin/social-meida/edit' === request()->path() ? 'active' : '' }}">
                        <i class="fas fa-share-alt nav-icon"></i>
                        <p>Social Media</p>
                    </a>
                </li>
            </ul>
        </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>