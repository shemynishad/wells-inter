<section class="section" id="media_section">
    <div class="container">
        <div class="heading_group">
            <span>{{ $content($id)->headline_1 }}</span>
            @if($content($id)->page_id == 15)
            <select class="media_drop">
                <option value="1">مصنف بواسطة</option>
                <option value="2">صحافة</option>
                <option value="4">تأملات</option>
                <option value="3">القصص</option>
                <!-- <option value="5">الجوائز</option> -->
                <!-- <option value="6">أرشيف</option> -->
            </select>
            @else
            <select class="media_drop">
                <option value="1">Filter By</option>
                <option value="2">Press</option>
                <option value="4">Musings</option>
                <option value="3">Stories</option>
                <!-- <option value="5">Awards</option> -->
                <!-- <option value="6">Archives</option> -->
            </select>
            @endif
        </div>
        <div class="media_menu"></div>
        @if($content($id)->page_id == 15)
        <div class="heading_group mt-2 no-items" id="no-items">
            <span>لا توجد عناصر في الوسائط</span>
        </div>
        @else
        <div class="heading_group mt-2 no-items" id="no-items">
            <span>No posts to show under this category</span>
        </div>
        @endif
        <div class="menu-box">
            <div class="row">
                @foreach($content($id)->media_archives as $archive)
                @if($archive->link)
                <div class="col-6 col-md-6 col-lg-2 filterDiv filter_{{ $archive->media_archive_category_id }} media_post" data-id="{{ $archive->id }}">
                    <a target="_blank" href="{{ $archive->link }}">
                        <img src="/images/media_archive/{{ $archive->thumbnail }}" class="img-fluid" width="203" height="270" alt="" />
                    </a>
                </div>
              @elseif($archive->video_url)
                <div class="col-6 col-md-6 col-lg-2 filterDiv filter_{{ $archive->media_archive_category_id }} media_post" data-id="{{ $archive->id }}">
                    <a href="//fast.wistia.net/embed/iframe/{{$archive->video_url}}?videoFoam=true&autoplay=true" data-fancybox data-type="iframe" data-width=930 data-height=520>
                        <img src="/images/media_archive/{{$archive->thumbnail}}" class=" img-fluid" width="203" height="270" alt="" />
                    </a>
                </div>
                @else @foreach($archive->media_archive_gallery as $gallery)
                @if($loop->iteration == 1)
                <div class="col-6 col-md-6 col-lg-2 filterDiv filter_{{ $archive->media_archive_category_id }} media_post" data-id="{{ $archive->id }}">
                    <a data-fancybox="gallery_{{ $gallery->media_archive_id }}" href="/images/media_archive/{{ $gallery->image}}">
                        <img src="/images/media_archive/{{ $archive->thumbnail }}" class="img-fluid" width="203" height="270" alt="" />
                    </a>
                </div>
                @else
                <div class="col-6 col-md-6 col-lg-2 filterDiv filter_{{ $archive->media_archive_category_id }} d-none media_post" data-id="{{ $archive->id }}">
                    <a data-fancybox="gallery_{{ $gallery->media_archive_id }}" href="/images/media_archive/{{ $gallery->image}}">
                        <img src="/images/media_archive/{{ $archive->thumbnail }}" class="img-fluid" width="203" height="270" alt="" />
                    </a>
                </div>
                @endif
                @endforeach
                @endif
                @endforeach
            </div>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.media_post').on('click', function() {
                var media_id = $(this).data("id");
                $.ajax({
                    type: "GET",
                    url: "{{route('media.count')}}",
                    data: {
                        'media_id': media_id
                    },
                    complete: function() {
                        console.log('success');
                    }
                });
            });
            filterSelection("all");
            $('.media_drop').on('change', function() {
                var val = this.value;
                if (val == 1) {
                    filterSelection('all');
                } else {
                    filterSelection('filter_' + val);
                }
            });
        });

        function filterSelection(c) {
            var x, i, noitems;
            x = document.getElementsByClassName("filterDiv");
            noitems = 0;
            if (c == "all") c = "";
            // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
            for (i = 0; i < x.length; i++) {
                w3RemoveClass(x[i], "show");
                if (x[i].className.indexOf(c) > -1) {
                    w3AddClass(x[i], "show");
                    noitems++;
                }
            }
            if (noitems == 0) {
                $('#no-items').addClass('show');
            } else {
                $('#no-items').removeClass('show');
            }
        }
        // Show filtered elements
        function w3AddClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                if (arr1.indexOf(arr2[i]) == -1) {
                    element.className += " " + arr2[i];
                }
            }
        }

        // Hide elements that are not selected
        function w3RemoveClass(element, name) {
            var i, arr1, arr2;
            arr1 = element.className.split(" ");
            arr2 = name.split(" ");
            for (i = 0; i < arr2.length; i++) {
                while (arr1.indexOf(arr2[i]) > -1) {
                    arr1.splice(arr1.indexOf(arr2[i]), 1);
                }
            }
            element.className = arr1.join(" ");
        }

        function popupOpen() {
            $('#video_div a').trigger('click');
        }
    </script>
</section>