<section class="section " id="experiance">
    <div class="container exp_inner"> 
        <span></span>
        <div class="row">
            @foreach($content($id)->experiences as $experience)
                <div data-aos="fade-up" class="col-12 col-md-6 col-lg-3 ah_exp_cls">
                    <h2 class="exp_headline"> {{ $experience->headline }} </h2>
                    <span class="exp_divider"> ................................. </span>
                    <h3 class="exp_sub_headline"> {!! nl2br($experience->sub_headline) !!} </h3>
                </div>
            @endforeach
        </div>
    </div>
</section>