<footer class="approach_footer">
    @if($content($id)->text_1 || $content($id)->headline_1)
        <div class="footer_bg_full">
            <div class="row">
                <div class="col-12">
                    @if($content($id)->headline_1)
                        <div class="layer">
                            <h1 class="stories slide_two font-cg fadeIn">{{ $content($id)->headline_1 }}</h1>
                        </div>
                    @endif
                    @if($content($id)->text_1)
                        <p> {!! nl2br($content($id)->text_1) !!} </p>
                    @endif
                </div>
            </div>
        </div>
    @endif
    <div class="hotels_bottom_nav"> 
        <span>{{ $content($id)->text_2 }}</span> 
        <a class="go_up" id="backToTop"> <img src="{{ asset('images') }}/icons/arrow_up_white.svg" alt=""/></a> 
    </div>
</footer>