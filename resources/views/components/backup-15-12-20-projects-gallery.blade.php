<div class="section" id="sectionhotels03">
    <div class="layer gallery">
        <div class="projects_gellery">
            @foreach($content($id)->projects as $project)
                @foreach($project->project as $image)
                    @if($image->image_class == 0)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="d-none" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects_gallery/{{ $image->image }}" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 1)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-1" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_01.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 2)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-2" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_02.jpg" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 3)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-3" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_03.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 4)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-4" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_04.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 5)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-5" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_05.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 6)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-6" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_06.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 7)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-7" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_07.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 8)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-8" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_08.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 9)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-9" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_44.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 10)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-10" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_45.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 11)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-11" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_09.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 12)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-12" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_10.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 13)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-13" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_11.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 14)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-14" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_12.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 15)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-15" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_46.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 16)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-16" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_13.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 17)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-17" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_14.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 18)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-18" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_15.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 19)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-19" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_16.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 20)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-20" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_17.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 21)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-21" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_18.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 22)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-22" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_19.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 23)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-23" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_20.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 24)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-24" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_21.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 25)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-25" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_22.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 26)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-26" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_23.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 27)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-27" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_24.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 28)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-28" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_25.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 29)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-29" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_26.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 30)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-30" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_27.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 31)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-31" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_28.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 32)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-32" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_29.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 33)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-33" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_30.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 34)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-34" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_31.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 35)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-35" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_32.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 36)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-36" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_33.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 37)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-37" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_34.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 38)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-38" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_35.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 39)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-39" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_36.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 40)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-40" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_37.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 41)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-41" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_38.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 42)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-42" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_39.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 43)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-43" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_40.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 44)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-44" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_41.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 45)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-45" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_42.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 46)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-46" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_47.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 47)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-47" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_48.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @elseif($image->image_class == 48)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="class-48" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects/project_imh_43.png" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @endif
                @endforeach
            @endforeach   
        </div>
        <!-- Root element of PhotoSwipe. Must have class pswp. -->
        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"> 
            <!-- Background of PhotoSwipe. 
            It's a separate element as animating opacity is faster than rgba(). -->
            <div class="pswp__bg"></div>
            <!-- Slides wrapper with overflow:hidden. -->
            <div class="pswp__scroll-wrap"> 
                <!-- Container that holds slides. 
                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                Don't modify these 3 pswp__item elements, data is added later on. -->
                <div class="pswp__container">
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                </div>
                <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                <div class="pswp__ui pswp__ui--hidden">
                    <div class="pswp__top-bar"> 
                        <!--  Controls are self-explanatory. Order can be changed. -->
                        <div class="pswp__counter"></div>
                        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                        <!--                  <button class="pswp__button pswp__button--share" title="Share"></button>--> 
                        <!--                  <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>--> 
                        <!--                  <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>--> 
                        <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR --> 
                        <!-- element will get class pswp__preloader--active when preloader is running -->
                        <div class="pswp__preloader">
                            <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                        <div class="pswp__share-tooltip"></div>
                    </div>
                    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"> </button>
                    <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"> </button>
                    <div class="pswp__caption">
                        <div class="pswp__caption__center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>