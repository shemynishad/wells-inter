    <!--<div class="se-pre-con"></div>-->
    <section class="main_menu" id="mainmenu" style="z-index:10000;">
        <div class="navMain">
            <nav class="navbar navbar-expand-lg">
                <div class="top_menu">
                    <ul>
                        <li><a target="_blank" href="{{ $content->instagram_link }}"><img src="images/icons/instagram.svg" alt="Instagram"></a></li>
                        <li><a target="_blank" href="{{ $content->pinterest_link }}"><img src="images/icons/pinterest.svg" alt="Pintrest"></a></li>
                        <li><a target="_blank" href="{{ $content->linkedin_link }}"><img src="images/icons/linkedin.svg" alt="LinkedIn"></a></li>
                        <li>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> en </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <button class="dropdown-item" type="button" onclick="location.href='/ar/{{Request::segment(1)}}'">ar</button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <svg width="22" height="24" viewBox="0 0 12 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="12" height="0.4" fill="#fff"/>
                        <rect y="4" width="12" height="0.4" fill="#fff"/>
                        <rect y="8" width="12" height="0.4" fill="#fff"/>
                    </svg>
                </button>
                <div class="mobile-phone">
                    <a href="tel:+971508868840"><svg width="18" height="28" viewBox="0 0 23 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.7424 30.5299C7.18206 25.0101 2.18994 16.3246 0.201203 8.73499C-1.21932 3.37759 5.31509 -2.06099 6.24858 0.780062L8.60259 8.1262C8.76493 8.73499 8.27789 9.34379 7.75027 9.66848C6.28916 10.4802 5.63978 10.0743 5.15274 10.3584C3.32636 11.4137 12.0118 26.5118 13.8382 25.416C14.3253 25.1319 14.3253 24.3608 15.7458 23.549C16.2734 23.2243 17.0446 23.1026 17.491 23.549L22.6861 29.2717C24.6748 31.504 16.7199 34.4668 12.7424 30.5299Z" fill="white"/>
                        </svg></a>
                </div>
                <div class="mobile-phone">
                    <div class="dropdown" style="top: 7px;">
                        <button class="btn dropdown-toggle" style="color:white;" type="button" id="dropdownMenu2"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> en </button>
                        <div class="dropdown-menu" style="padding-top: 20px;background-color:#fff0; border:0px solid rgba(0,0,0,.15);margin:-13px;" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" style="color:white; font-size:14px" type="button" onclick="location.href='/ar/{{Request::segment(1)}}'">ar</button>
                        </div>
                    </div>
                </div>
                <div class="nav_flex_2"> 
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="images/logo-nav.png" width="116" height="59" alt="" />
                    </a> 
                </div>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <div class="nav_flex_1">
                        <ul class="navbar-nav">
                            <li class="nav-item active"> <a class="nav-link" href="{{ url('/hotels') }}">HOTELS</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ url('/fnb') }}">F & B</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ url('/spa') }}">SPA</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ url('collections') }}">COLLECTIONS</a> </li>
                        </ul>
                    </div>
                    <div class="nav_flex_3">
                        <ul class="navbar-nav float-right">
                            <li class="nav-item active"> <a class="nav-link" href="{{ url('approach') }}">APPROACH</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ url('manifesto') }}">MANIFESTO</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ url('projects') }}">PROJECTS</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ url('contact') }}">CONTACT</a> </li>
                        </ul>
                    </div>
                    <div class="socials" style="display: none;">
                        
                        <ul id="lpro" style="text-align: center;">
                            <li style="margin: 0px 8px 10px 0px;"><a href="{{ $content->instagram_link }}" target="_blank"><div><img src="images/icons/instagram.svg" alt="Instagram"></a>
                            </li>
                            <li style="margin: 0px 8px 10px 0px;"><a href="{{ $content->pinterest_link }}" target="_blank"><div><img src="images/icons/pinterest.svg" alt="Pintrest"></a>
                            </li>
                            <li><a href="{{ $content->linkedin_link }}" target="_blank"><img src="images/icons/linkedin.svg" alt="LinkedIn"></a>
                            </li>
                        </ul>
                        
                        <style>
                        
                            ul li {
                            
                                display:inline-block;
                            
                            }
                        </style>
                        
                        <!--<div class="nav_socials_1" style="padding-right:4px;">-->
                        <!--    <a href="{{ $content->instagram_link }}"><img src="images/icons/instagram.svg" alt="Instagram"></a>-->
                        <!--</div>-->
                        <!--<div class="nav_socials_2" style="padding-left:4px;">-->
                        <!--    <a href="{{ $content->pinterest_link }}"><img src="images/icons/pinterest.svg" alt="Pintrest"></a>-->
                        <!--</div>-->
                        <!--<div class="nav_socials_3" style="padding-left:4px;">-->
                        <!--    <a href="{{ $content->linkedin_link }}"><img src="images/icons/linkedin.png" alt="LinkedIn"></a>-->
                        <!--</div>-->
                    </div>
                </div>
            </nav>
        </div>
    </section>