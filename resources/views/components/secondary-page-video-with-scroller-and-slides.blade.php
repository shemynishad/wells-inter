<div class="section" id="sectionhotels">
   <video id="myVideo" class="mobile_video" onplay="myFunction()" data-autoplay autoplay loop muted playsinline controls>
        <source src="{{ asset('videos') }}/{{ $content($id)->video_1 }}" type="video/mp4">
        <source src="{{ asset('videos') }}/{{ $content($id)->video_2 }}" type="video/webm">
    </video>
    <div id="myVideo" class="desktop_video">{!! $content($id)->embed_video !!}</div>
    @if($content($id)->page_id == 6 || $content($id)->page_id == 8 || $content($id)->page_id == 15 || $content($id)->page_id == 17)
    <div class="layer hotels-innner">
        <h1 class="slide_two font-cg hotels" id="approach01" style="padding-top: 20px; display:none;">{{ $content($id)->input_1 }}</h1>
        <h1 class="slide_two  font-cg hotels_small" id="approach02" style="display:none;">{{ $content($id)->input_2 }}</h1>
        <div class="row hotel02">
            <div class="hotels_inner_bg" id="approach03" style="display:none;">
                <p>{!! nl2br($content($id)->text_1) !!}</p>
            </div>
            <div class="bounce">
                <a href="#sectionhotels03" id="myBtn" title="Go to top" class="arrow_down_hotel "></a>
            </div>
        </div>
    </div>
    @else
    <div class="layer hotels-innner">
        <h1 class="slide_two font-cg hotels" id="hotels01" style="padding-top: 20px; display:none;">{{ $content($id)->input_1 }}</h1>
        <h1 class="slide_two  font-cg hotels_small" id="hotels02" style="display:none;">{{ $content($id)->input_2 }}</h1>
        <div class="row hotel02">
            <div class="hotels_inner_bg" id="hotels03" style="display:none;">
                <p>{!! nl2br($content($id)->text_1) !!}</p>
            </div>
            <div class="bounce">
                <a href="#sectionhotels03" id="myBtn" title="Go to top" class="arrow_down_hotel"></a>
            </div>
        </div>
    </div>
    @endif
</div>