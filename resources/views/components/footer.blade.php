<footer class="approach_footer">
    @if($content($id)->text_1)
        <div class="footer_bg_full">
            <div class="row">
                <div class="col-12">
                    <p> {!! nl2br($content($id)->text_1) !!} </p>
                </div>
            </div>
        </div>
    @endif
    <div class="hotels_bottom_nav"> 
        <span>{{ $content($id)->text_2 }}</span> 
        <a class="go_up" id="backToTop"> <img src="images/icons/arrow_up_white.svg" alt=""/></a> 
    </div>
</footer>