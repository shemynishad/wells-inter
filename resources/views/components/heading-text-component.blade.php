<section class="section" id="people_section" data-aos="fade-up">
    <div class="container">
        <div class="col-12">
            <div class="heading_group">
                <span>{{ $content($id)->headline_1 }}</span>
                <h2>{{ $content($id)->headline_2 }}</h2>
            </div>
            <div class="row">
                <h3>“{{ $content($id)->input_1 }}”</h3>
                <p>{{ $content($id)->text_1 }}</p>
            </div>
        </div>
    </div>
</section>