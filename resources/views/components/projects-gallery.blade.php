<div class="section" id="sectionhotels03">
    <div class="layer gallery">
        <div class="projects_gellery">
            @foreach($content($id)->projects as $project)
                @foreach($project->project as $image)
                    @if($image->image_class == 49)
                        <a data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->iteration }} / {{ $loop->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" class="d-none" title="{{ $project->project_name }}" href="/images/projects_gallery/{{ $image->image }}"> 
                            <img src="/images/projects_gallery/{{ $image->image }}" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                        </a>
                    @else
                        @foreach($project->project_slot as $slot)
                            @if($slot->slot_id == $image->image_class)
                                <a  class="class-{{ $slot->slot_id }}" data-caption="<div class='d-flex justify-content-between'><div><span class='mr-3'> {{ $loop->parent->iteration }} / {{ $loop->parent->count }} </span>{{ $project->project_name }}</div><div>{{ $image->image_caption }}</div></div>" data-fancybox="gallery" href="/images/projects_gallery/{{ $image->image }}"> 
                                    <img src="/images/projects_slots/{{ $slot->thumbnail }}" alt="{{ $image->image_caption }}" class="item" data-aos="fade-up"/> 
                                </a>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endforeach   
        </div>
    </div>
</div>