<div class="section " id="section0">
    <video id="myVideo" data-autoplay autoplay muted loop playsinline controls>
        <source src="videos/{{ $content($id)->video_1 }}" type="video/mp4">
        <source src="videos/{{ $content($id)->video_2 }}" type="video/webm">
    </video>
    <!--<div id="myVideo1" class="desktop_video">{!! $content($id)->embed_video !!}</div>-->
    <div style="display:none" id="embed_wistia_video">{!! $content($id)->embed_video !!}</div>
    <div class="layer">
        <ul class="sub-menu" id="dropDownMenu">
            <li id="homeid1"><img src="images/logo.svg" width="596" height="130" alt="" class="home_text1" /></li>
            <li id="homeid2"><img src="images/logo-2.svg" width="467" height="11" alt="" class="home_text2" /></li>
            <li id="homeid3"><h1 class="slide_two font-cg fadeIn">{{ $content($id)->input_1 }}</h1></li>
            <li id="homeid4"><h1 class="fontlarge slide_two font-cg fadeIn">{{ $content($id)->input_2 }}</h1></li>
            <li id="homeid5"><h1 class="fontlarge slide_two font-cg  fadeIn">{{ $content($id)->input_3 }}</h1></li>
            <li id="homeid6"><h1 class="fontlarge slide_two font-cg  fadeIn">{{ $content($id)->input_4 }}</h1></li>
            <li id="homeid7"><h1 class="fontmedium slide_two font-cg fadeIn">{{ $content($id)->input_5 }}</h1></li>
            <li id="homeid8"><h1 class="stories slide_two font-cg fadeIn">{{ $content($id)->input_6 }}</h1></li>
        </ul>
    </div>
    <button onclick="enableMute()" type="button" class="mute_btn">
        <img src="images/icons/mute.png" class="mute-icon" alt=""/>
        <img src="images/icons/unmute-icon.svg" class="unmute-icon" alt=""/>
    </button>
</div>