<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Home</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @foreach($tt_contents as $content)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $content->component->title }}</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-md-8 order-md-2">
                                <div class="section-inner-txt order-md-2">
                                    <h3>
                                    {{ $content->component->title }}
                                    </h3>
                                    {{ $content->component->description }}
                                </div>
                            </div>
                            <div class="col-md-4 order-md-1">
                                <div class="section-inner-img">
                                    <img src="{{ asset('images')}}/{{ $content->component->thumbnail }}">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="">
                            @if($content->component->id != 5 && $content->component->id != 12)
                                <a href="{{ url('admin/component') }}/{{ $content->id }}/edit" class="btn btn-primary"> Edit {{ $content->component->title }} </a>
                            @endif
                            @if($content->component->id == 3)
                                <a href="/admin/{{ $content->id }}/carousal" class="btn btn-primary"> Carousal Slides </a>
                            @elseif($content->component->id == 5)
                                <a href="/admin/{{ $content->id }}/experience" class="btn btn-primary"> Experiences </a>
                            @elseif($content->component->id == 6)
                                <a href="/admin/{{ $content->id }}/brands" class="btn btn-primary"> Brands </a>
                            @elseif($content->component->id == 8)
                                <a href="/admin/{{ $content->id }}/media-archive-category" class="btn btn-primary"> Categories </a>
                                <a href="/admin/{{ $content->id }}/media-archive" class="btn btn-primary"> Media Archive </a>
                            @elseif($content->component->id == 11)
                                <a href="/admin/{{ $content->id }}/manifesto-slide" class="btn btn-primary"> Manifesto Slides </a>
                            @elseif($content->component->id == 12)
                                <a href="/admin/{{ $content->id }}/project" class="btn btn-primary"> Projects </a>
                            @endif
                            
                        </div>
                    </div>
                    <!-- /.card-footer-->
                </div>
                <!-- /.card -->
            @endforeach
        </section>
    </main>
</x-master-admin-layout>
