<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Edit Component</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        @if($tt_content->page_id > 9)
        <section class="content admin_arabic_layout">
            @else
            <section class="content">
                @endif
                <!-- Default box -->
                @if($tt_content->component_id == 1)

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <div class="form-group row">
                            {!! Form::label('homePageBackgroundMp4Video', 'Homepage MP4 Video Upload', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="input-group col-sm-9">
                                <div class="custom-file">
                                    {!! Form::file('video_mp4',['class' => 'custom-file-input', 'id' => 'homePageBackgroundMp4Video']) !!}
                                    {!! Form::label('homePageBackgroundMp4Video', 'Choose Video', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('homePageBackgroundWebmVideo', 'Homepage WEBM Video Upload', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="input-group col-sm-9">
                                <div class="custom-file">
                                    {!! Form::file('video_webm',['class' => 'custom-file-input', 'id' => 'homePageBackgroundWebmVideo']) !!}
                                    {!! Form::label('homePageBackgroundWebmVideo', 'Choose Video', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('homePageBackgroundMp4Video', 'Homepage Wistia Video Embed Code', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('embed_video', null , ['placeholder' => 'Video inline embed code', 'class' => 'form-control', 'id' => 'homePageBackgroundMp4Video']) !!}
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs font-bold">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div id="english_content" class="tab-pane active">
                            <div class="form-group row">
                                {!! Form::label('inputSlide_1', 'Headline 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1', null , ['placeholder' => 'Slide 1', 'class' => 'form-control', 'id' => 'inputSlide_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputSlide_2', 'Headline 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_2', null , ['placeholder' => 'Slide 2', 'class' => 'form-control', 'id' => 'inputSlide_2']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputSlide_3', 'Headline 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_3', null , ['placeholder' => 'Slide 3', 'class' => 'form-control', 'id' => 'inputSlide_3']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputSlide_4', 'Headline 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_4', null , ['placeholder' => 'Slide 4', 'class' => 'form-control', 'id' => 'inputSlide_4']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputSlide_5', 'Headline 5', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_5', null , ['placeholder' => 'Slide 5', 'class' => 'form-control', 'id' => 'inputSlide_5']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputSlide_6', 'Headline 6', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_6', null , ['placeholder' => 'Slide 6', 'class' => 'form-control', 'id' => 'inputSlide_6']) !!}
                                </div>
                            </div>
                        </div>
                        <div id="arabic_content" style="display: none;">
                            <div class="form-group row">
                                {!! Form::label('inputSlide_1', 'Headline 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1_ar', null , ['placeholder' => 'Slide 1', 'class' => 'form-control ar_input', 'id' => 'inputSlide_1']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputSlide_2', 'Headline 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_2_ar', null , ['placeholder' => 'Slide 2', 'class' => 'form-control ar_input', 'id' => 'inputSlide_2']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputSlide_3', 'Headline 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_3_ar', null , ['placeholder' => 'Slide 3', 'class' => 'form-control ar_input', 'id' => 'inputSlide_3']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputSlide_4', 'Headline 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_4_ar', null , ['placeholder' => 'Slide 4', 'class' => 'form-control ar_input', 'id' => 'inputSlide_4']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputSlide_5', 'Headline 5', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_5_ar', null , ['placeholder' => 'Slide 5', 'class' => 'form-control ar_input', 'id' => 'inputSlide_5']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputSlide_6', 'Headline 6', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_6_ar', null , ['placeholder' => 'Slide 6', 'class' => 'form-control ar_input', 'id' => 'inputSlide_6']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                @elseif($tt_content->component_id == 2)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <div class="form-group row">
                            {!! Form::label('secondaryPageBackgroundMp4Video', 'Upload MP4 Video', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="input-group col-sm-9">
                                <div class="custom-file">
                                    {!! Form::file('video_mp4',['class' => 'custom-file-input', 'id' => 'secondaryPageBackgroundMp4Video']) !!}
                                    {!! Form::label('secondaryPageBackgroundMp4Video', 'Choose Video', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('secondaryPageBackgroundWebmVideo', 'Upload WEBM Video', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="input-group col-sm-9">
                                <div class="custom-file">
                                    {!! Form::file('video_webm',['class' => 'custom-file-input', 'id' => 'secondaryPageBackgroundWebmVideo']) !!}
                                    {!! Form::label('secondaryPageBackgroundWebmVideo', 'Choose Video', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('secondaryPageBackgroundMp4Video', 'Wistia Video Embed Code', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('embed_video', null , ['placeholder' => 'Video inline embed code', 'class' => 'form-control', 'id' => 'secondaryPageBackgroundMp4Video']) !!}
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs font-bold">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div id="english_content" class="tab-pane active">

                            <div class="form-group row">
                                {!! Form::label('inputSlide_1', 'Headline 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1', null , ['placeholder' => 'Slide 1', 'class' => 'form-control', 'id' => 'inputSlide_1']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputSlide_2', 'Headline 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_2', null , ['placeholder' => 'Slide 2', 'class' => 'form-control', 'id' => 'inputSlide_2']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputText_1', 'Description', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1', null , ['placeholder' => 'Text', 'class' => 'form-control', 'id' => 'inputText_1']) !!}
                                </div>
                            </div>
                        </div>
                        <div id="arabic_content" class="tab-pane" style="display:none;">
                            <div class="form-group row">
                                {!! Form::label('inputSlide_1', 'Headline 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1_ar', null , ['placeholder' => 'Slide 1', 'class' => 'form-control ar_input', 'id' => 'inputSlide_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputSlide_2', 'Headline 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_2_ar', null , ['placeholder' => 'Slide 2', 'class' => 'form-control ar_input', 'id' => 'inputSlide_2']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputText_1', 'Description', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1_ar', null , ['placeholder' => 'Text', 'class' => 'form-control ar_input', 'id' => 'inputText_1']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                @elseif($tt_content->component_id == 3)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <ul class="nav nav-tabs font-bold">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                        </ul>
                        <br />
                        <div id="english_content" class="tab-pane active">
                            <div class="form-group row">
                                {!! Form::label('carousalHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1', null , ['placeholder' => 'Headline', 'class' => 'form-control', 'id' => 'carousalHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('carousalMainHeadline', 'Main Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2', null , ['placeholder' => 'Main Headline', 'class' => 'form-control', 'id' => 'carousalMainHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('carousalSubHeadline', 'Sub Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_3', null , ['placeholder' => 'Sub Headline', 'class' => 'form-control', 'id' => 'carousalSubHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputText_1', 'Text', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1', null , ['placeholder' => 'Text', 'class' => 'form-control', 'id' => 'inputText_1']) !!}
                                </div>
                            </div>
                        </div>
                        <div id="arabic_content" class="tab-pane" style="display: none;">
                            <div class="form-group row">
                                {!! Form::label('carousalHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1_ar', null , ['placeholder' => 'Headline', 'class' => 'form-control ar_input', 'id' => 'carousalHeadline']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('carousalMainHeadline', 'Main Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2_ar', null , ['placeholder' => 'Main Headline', 'class' => 'form-control ar_input', 'id' => 'carousalMainHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('carousalSubHeadline', 'Sub Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_3_ar', null , ['placeholder' => 'Sub Headline', 'class' => 'form-control ar_input', 'id' => 'carousalSubHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputText_1', 'Text', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1_ar', null , ['placeholder' => 'Text', 'class' => 'form-control ar_input', 'id' => 'inputText_1']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                @elseif($tt_content->component_id == 4)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <ul class="nav nav-tabs font-bold">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                        </ul>
                        <br />
                        <div id="english_content" class="tab-pane active">
                            <div class="form-group row">
                                {!! Form::label('inputFooterHeadline', 'Footer Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1', null , ['placeholder' => 'Footer Headline', 'class' => 'form-control', 'id' => 'inputFooterHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputText_1', 'Footer Text With Background', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1', null , ['placeholder' => 'Footer Text With Background', 'class' => 'form-control', 'id' => 'inputText_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputText_2', 'Footer Text Without Background', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_2', null , ['placeholder' => 'Footer Text Without Background', 'class' => 'form-control', 'id' => 'inputText_2']) !!}
                                </div>
                            </div>
                        </div>
                        <div id="arabic_content" class="tab-pane" style="display: none;">
                            <div class="form-group row">
                                {!! Form::label('inputFooterHeadline', 'Footer Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1_ar', null , ['placeholder' => 'Footer Headline', 'class' => 'form-control ar_input', 'id' => 'inputFooterHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputText_1', 'Footer Text With Background', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1_ar', null , ['placeholder' => 'Footer Text With Background', 'class' => 'form-control ar_input', 'id' => 'inputText_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputText_2', 'Footer Text Without Background', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_2_ar', null , ['placeholder' => 'Footer Text Without Background', 'class' => 'form-control ar_input', 'id' => 'inputText_2']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                @elseif($tt_content->component_id == 6)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <ul class="nav nav-tabs font-bold">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                        </ul>
                        <br />
                        <div id="english_content" class="tab-pane active">
                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Heading', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1', null , ['placeholder' => 'Sub Heading', 'class' => 'form-control', 'id' => 'inputSubHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2', null , ['placeholder' => 'Headline', 'class' => 'form-control', 'id' => 'inputHeadline']) !!}
                                </div>
                            </div>
                        </div>
                        <div id="arabic_content" class="tab-pane" style="display: none;">
                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Heading', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1_ar', null , ['placeholder' => 'Sub Heading', 'class' => 'form-control ar_input', 'id' => 'inputSubHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2_ar', null , ['placeholder' => 'Headline', 'class' => 'form-control ar_input', 'id' => 'inputHeadline']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                @elseif($tt_content->component_id == 7)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <ul class="nav nav-tabs font-bold">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                        </ul>
                        <br />
                        <div id="english_content" class="tab-pane active">
                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Heading', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1', null , ['placeholder' => 'Sub Heading', 'class' => 'form-control', 'id' => 'inputSubHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2', null , ['placeholder' => 'Headline', 'class' => 'form-control', 'id' => 'inputHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputQuote', 'Quote', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1', null , ['placeholder' => 'Quote', 'class' => 'form-control', 'id' => 'inputQuote']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputText', 'Text', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1', null , ['placeholder' => 'Text', 'class' => 'form-control', 'id' => 'inputText']) !!}
                                </div>
                            </div>s
                        </div>
                        <div id="arabic_content" class="tab-pane" style="display: none;">

                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Heading(AR)', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1_ar', null , ['placeholder' => 'Sub Heading', 'class' => 'form-control ar_input', 'id' => 'inputSubHeadline']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2_ar', null , ['placeholder' => 'Headline', 'class' => 'form-control ar_input', 'id' => 'inputHeadline']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputQuote', 'Quote', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1_ar', null , ['placeholder' => 'Quote', 'class' => 'form-control ar_input', 'id' => 'inputQuote']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputText', 'Text', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1_ar', null , ['placeholder' => 'Text', 'class' => 'form-control ar_input', 'id' => 'inputText']) !!}
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                @elseif($tt_content->component_id == 8)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <div class="form-group row">
                            {!! Form::label('inputHeadline', 'Headline(EN)', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('headline_1', null , ['placeholder' => 'Headline', 'class' => 'form-control', 'id' => 'inputHeadline']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputHeadline', 'Headline(AR)', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('headline_1_ar', null , ['placeholder' => 'Headline', 'class' => 'form-control ar_input', 'id' => 'inputHeadline']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                @elseif($tt_content->component_id == 9)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}

                        <ul class="nav nav-tabs font-bold">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                        </ul>
                        <br />
                        <div id="english_content" class="tab-pane active">
                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Heading', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1', null , ['placeholder' => 'Sub Heading', 'class' => 'form-control', 'id' => 'inputSubHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2', null , ['placeholder' => 'Headline', 'class' => 'form-control', 'id' => 'inputHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputImage_1', 'Expertise Image 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    <div class="input-group col-sm-12">
                                        <div class="custom-file">
                                            {!! Form::file('image_1',['class' => 'custom-file-input', 'id' => 'inputImage_1']) !!}
                                            {!! Form::label('inputImage_1', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                        </div>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('image_1') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_1', 'Expertise Heading 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_3', null , ['placeholder' => 'Expertise Heading 1', 'class' => 'form-control', 'id' => 'inputExpertiseHeading_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_1', 'Text 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1', null , ['placeholder' => 'Text 1', 'class' => 'form-control', 'id' => 'inputExpertiseText_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputImage_2', 'Expertise Image 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    <div class="input-group col-sm-12">
                                        <div class="custom-file">
                                            {!! Form::file('image_2',['class' => 'custom-file-input', 'id' => 'inputImage_2']) !!}
                                            {!! Form::label('inputImage_2', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                        </div>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('image_2') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_2', 'Expertise Heading 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_4', null , ['placeholder' => 'Expertise Heading 2', 'class' => 'form-control', 'id' => 'inputExpertiseHeading_2']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_2', 'Text 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_2', null , ['placeholder' => 'Text 2', 'class' => 'form-control', 'id' => 'inputExpertiseText_3']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputImage_3', 'Expertise Image 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    <div class="input-group col-sm-12">
                                        <div class="custom-file">
                                            {!! Form::file('image_3',['class' => 'custom-file-input', 'id' => 'inputImage_3']) !!}
                                            {!! Form::label('inputImage_3', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                        </div>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('image_3') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_3', 'Expertise Heading 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_5', null , ['placeholder' => 'Expertise Heading 3', 'class' => 'form-control', 'id' => 'inputExpertiseHeading_3']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_3', 'Text 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_3', null , ['placeholder' => 'Text 1', 'class' => 'form-control', 'id' => 'inputExpertiseText_3']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputImage_4', 'Expertise Image 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    <div class="input-group col-sm-12">
                                        <div class="custom-file">
                                            {!! Form::file('image_4',['class' => 'custom-file-input', 'id' => 'inputImage_4']) !!}
                                            {!! Form::label('inputImage_4', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                        </div>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('image_4') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_4', 'Expertise Heading 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_6', null , ['placeholder' => 'Expertise Heading 4', 'class' => 'form-control', 'id' => 'inputExpertiseHeading_4']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_4', 'Text 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_4', null , ['placeholder' => 'Text 4', 'class' => 'form-control', 'id' => 'inputExpertiseText_4']) !!}
                                </div>
                            </div>

                        </div>
                        <div id="arabic_content" class="tab-pane" style="display: none;">

                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Heading', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1_ar', null , ['placeholder' => 'Sub Heading', 'class' => 'form-control ar_input', 'id' => 'inputSubHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2_ar', null , ['placeholder' => 'Headline', 'class' => 'form-control ar_input', 'id' => 'inputHeadline']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_1', 'Expertise Heading 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_3_ar', null , ['placeholder' => 'Expertise Heading 1', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseHeading_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_1', 'Text 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1_ar', null , ['placeholder' => 'Text 1', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseText_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_2', 'Expertise Heading 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_4_ar', null , ['placeholder' => 'Expertise Heading 2', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseHeading_2']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_2', 'Text 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_2_ar', null , ['placeholder' => 'Text 2', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseText_3']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_3', 'Expertise Heading 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_5_ar', null , ['placeholder' => 'Expertise Heading 3', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseHeading_3']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_3', 'Text 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_3_ar', null , ['placeholder' => 'Text 1', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseText_3']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_4', 'Expertise Heading 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_6_ar', null , ['placeholder' => 'Expertise Heading 4', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseHeading_4']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_4', 'Text 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_4_ar', null , ['placeholder' => 'Text 4', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseText_4']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                @elseif($tt_content->component_id == 10)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <ul class="nav nav-tabs font-bold">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                            </li>
                        </ul>
                        <br />
                        <div id="english_content" class="tab-pane active">

                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Heading', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1', null , ['placeholder' => 'Sub Heading', 'class' => 'form-control', 'id' => 'inputSubHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2', null , ['placeholder' => 'Headline', 'class' => 'form-control', 'id' => 'inputHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputQuote', 'Quote', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1', null , ['placeholder' => 'Quote', 'class' => 'form-control', 'id' => 'inputQuote']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputImage_1', 'Expertise Image 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    <div class="input-group col-sm-12">
                                        <div class="custom-file">
                                            {!! Form::file('image_1',['class' => 'custom-file-input', 'id' => 'inputImage_1']) !!}
                                            {!! Form::label('inputImage_1', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                        </div>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('image_1') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_1', 'Expertise Heading 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_3', null , ['placeholder' => 'Expertise Heading 1', 'class' => 'form-control', 'id' => 'inputExpertiseHeading_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_1', 'Text 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1', null , ['placeholder' => 'Text 1', 'class' => 'form-control', 'id' => 'inputExpertiseText_1']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputImage_2', 'Expertise Image 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    <div class="input-group col-sm-12">
                                        <div class="custom-file">
                                            {!! Form::file('image_2',['class' => 'custom-file-input', 'id' => 'inputImage_2']) !!}
                                            {!! Form::label('inputImage_2', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                        </div>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('image_2') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_2', 'Expertise Heading 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_4', null , ['placeholder' => 'Expertise Heading 2', 'class' => 'form-control', 'id' => 'inputExpertiseHeading_2']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_2', 'Text 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_2', null , ['placeholder' => 'Text 2', 'class' => 'form-control', 'id' => 'inputExpertiseText_3']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputImage_3', 'Expertise Image 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    <div class="input-group col-sm-12">
                                        <div class="custom-file">
                                            {!! Form::file('image_3',['class' => 'custom-file-input', 'id' => 'inputImage_3']) !!}
                                            {!! Form::label('inputImage_3', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                        </div>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('image_3') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_3', 'Expertise Heading 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_5', null , ['placeholder' => 'Expertise Heading 3', 'class' => 'form-control', 'id' => 'inputExpertiseHeading_3']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_3', 'Text 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_3', null , ['placeholder' => 'Text 1', 'class' => 'form-control', 'id' => 'inputExpertiseText_3']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputImage_4', 'Expertise Image 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    <div class="input-group col-sm-12">
                                        <div class="custom-file">
                                            {!! Form::file('image_4',['class' => 'custom-file-input', 'id' => 'inputImage_4']) !!}
                                            {!! Form::label('inputImage_4', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                        </div>
                                    </div>
                                    <span class="text-danger">{{ $errors->first('image_4') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_4', 'Expertise Heading 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_6', null , ['placeholder' => 'Expertise Heading 4', 'class' => 'form-control', 'id' => 'inputExpertiseHeading_4']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_4', 'Text 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_4', null , ['placeholder' => 'Text 4', 'class' => 'form-control', 'id' => 'inputExpertiseText_4']) !!}
                                </div>
                            </div>


                        </div>
                        <div id="arabic_content" class="tab-pane" style="display:none">

                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Heading', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1_ar', null , ['placeholder' => 'Sub Heading', 'class' => 'form-control ar_input', 'id' => 'inputSubHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2_ar', null , ['placeholder' => 'Headline', 'class' => 'form-control ar_input', 'id' => 'inputHeadline']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputQuote', 'Quote', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1_ar', null , ['placeholder' => 'Quote', 'class' => 'form-control ar_input', 'id' => 'inputQuote']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_1', 'Expertise Heading 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_3_ar', null , ['placeholder' => 'Expertise Heading 1', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseHeading_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_1', 'Text 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1_ar', null , ['placeholder' => 'Text 1', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseText_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_2', 'Expertise Heading 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_4_ar', null , ['placeholder' => 'Expertise Heading 2', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseHeading_2']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_2', 'Text 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_2_ar', null , ['placeholder' => 'Text 2', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseText_3']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_3', 'Expertise Heading 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_5_ar', null , ['placeholder' => 'Expertise Heading 3', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseHeading_3']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_3', 'Text 3', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_3_ar', null , ['placeholder' => 'Text 1', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseText_3']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseHeading_4', 'Expertise Heading 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_6_ar', null , ['placeholder' => 'Expertise Heading 4', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseHeading_4']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputExpertiseText_4', 'Text 4', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_4_ar', null , ['placeholder' => 'Text 4', 'class' => 'form-control ar_input', 'id' => 'inputExpertiseText_4']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.card-body -->
                </div>
                @elseif($tt_content->component_id == 11)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <div class="form-group row">
                            {!! Form::label('homePageBackgroundMp4Video', 'Home Page MP4 video', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="input-group col-sm-9">
                                <div class="custom-file">
                                    {!! Form::file('video_mp4',['class' => 'custom-file-input', 'id' => 'homePageBackgroundMp4Video']) !!}
                                    {!! Form::label('homePageBackgroundMp4Video', 'Choose Video', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('homePageBackgroundWebmVideo', 'Home Page WEBM video', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="input-group col-sm-9">
                                <div class="custom-file">
                                    {!! Form::file('video_webm',['class' => 'custom-file-input', 'id' => 'homePageBackgroundWebmVideo']) !!}
                                    {!! Form::label('homePageBackgroundWebmVideo', 'Choose Video', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('homePageBackgroundMp4Video', 'Home Page video embed code', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('embed_video', null , ['placeholder' => 'Video inline embed code', 'class' => 'form-control', 'id' => 'homePageBackgroundMp4Video']) !!}
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs font-bold">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div id="english_content" class="tab-pane active">
                            <div class="form-group row">
                                {!! Form::label('inputSlide_1', 'Slide 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1', null , ['placeholder' => 'Slide 1', 'class' => 'form-control', 'id' => 'inputSlide_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputSlide_2', 'Slide 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_2', null , ['placeholder' => 'Slide 2', 'class' => 'form-control', 'id' => 'inputSlide_2']) !!}
                                </div>
                            </div>
                            <h3 class="text-center"> Dynamic Content </h3>
                            <hr>
                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1', null , ['placeholder' => 'Sub Headline', 'class' => 'form-control', 'id' => 'inputSubHeadline']) !!}
                                    <span class="text-danger">{{ $errors->first('headline_1') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2', null , ['placeholder' => 'Headline(EN)', 'class' => 'form-control', 'id' => 'inputHeadline']) !!}
                                    <span class="text-danger">{{ $errors->first('headline_2') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputQuote', 'Quote', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_3', null , ['placeholder' => 'Quote', 'class' => 'form-control', 'id' => 'inputQuote']) !!}
                                    <span class="text-danger">{{ $errors->first('input_3') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputRteText', 'Text', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1', null , ['placeholder' => 'Text', 'class' => 'form-control', 'id' => 'inputRteText']) !!}
                                    <span class="text-danger">{{ $errors->first('text_1') }}</span>
                                </div>
                            </div>

                        </div>
                        <div id="arabic_content" class="tab-pane" style="display: none;">
                            <div class="form-group row">
                                {!! Form::label('inputSlide_1', 'Slide 1', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1_ar', null , ['placeholder' => 'Slide 1', 'class' => 'form-control ar_input', 'id' => 'inputSlide_1']) !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputSlide_2', 'Slide 2', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_2_ar', null , ['placeholder' => 'Slide 2', 'class' => 'form-control ar_input', 'id' => 'inputSlide_2']) !!}
                                </div>
                            </div>
                            <h3 class="text-center"> محتوى ديناميكي </h3>
                            <hr>
                            <div class="form-group row">
                                {!! Form::label('inputSubHeadline', 'Sub Headline(AR)', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_1_ar', null , ['placeholder' => 'Sub Headline', 'class' => 'form-control ar_input', 'id' => 'inputSubHeadline']) !!}
                                    <span class="text-danger">{{ $errors->first('headline_1_ar') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputHeadline', 'Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('headline_2_ar', null , ['placeholder' => 'Headline', 'class' => 'form-control ar_input', 'id' => 'inputHeadline']) !!}
                                    <span class="text-danger">{{ $errors->first('headline_2_ar') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputQuote', 'Quote', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_3_ar', null , ['placeholder' => 'Quote', 'class' => 'form-control ar_input', 'id' => 'inputQuote']) !!}
                                    <span class="text-danger">{{ $errors->first('input_3_ar') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputRteText', 'Text', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::textarea('text_1_ar', null , ['placeholder' => 'Text', 'class' => 'form-control ar_input', 'id' => 'inputRteTextAr']) !!}
                                    <span class="text-danger">{{ $errors->first('text_1_ar') }}</span>
                                </div>
                            </div>v


                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-sm-3 col-sm-9">
                            {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.card-body -->
                </div>
                <!-- /.card -->
                @elseif($tt_content->component_id == 13)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $tt_content->component->title }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::model($tt_content, ['method' => 'put', 'route' => ['admin.component.update', $tt_content->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id_ar', $tt_content->tt_content_id_ar) !!}
                        <div class="form-group row">
                            {!! Form::label('homePageBackgroundMp4Video', 'Home Page MP4 video', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="input-group col-sm-9">
                                <div class="custom-file">
                                    {!! Form::file('video_mp4',['class' => 'custom-file-input', 'id' => 'homePageBackgroundMp4Video']) !!}
                                    {!! Form::label('homePageBackgroundMp4Video', 'Choose Video', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('homePageBackgroundWebmVideo', 'Home Page WEBM video', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="input-group col-sm-9">
                                <div class="custom-file">
                                    {!! Form::file('video_webm',['class' => 'custom-file-input', 'id' => 'homePageBackgroundWebmVideo']) !!}
                                    {!! Form::label('homePageBackgroundWebmVideo', 'Choose Video', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('homePageBackgroundMp4Video', 'Home Page video embed code', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('embed_video', null , ['placeholder' => 'Video inline embed code', 'class' => 'form-control', 'id' => 'homePageBackgroundMp4Video']) !!}
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs font-bold">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div id="english_content" class="tab-pane active">
                            <div class="form-group row">
                                {!! Form::label('inputBusinessInquires', 'Business Inquiry Admin Email', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1', null , ['placeholder' => 'Business Inquiry Admin Email', 'class' => 'form-control', 'id' => 'inputBusinessInquires']) !!}
                                    <span class="text-danger">{{ $errors->first('input_1') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputGeneralInquires', 'General Inquiry Admin Email', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_2', null , ['placeholder' => 'General Inquiry Admin Email', 'class' => 'form-control', 'id' => 'inputGeneralInquires']) !!}
                                    <span class="text-danger">{{ $errors->first('input_2') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('inputCareerInquires', 'Career Inquiry Admin Email', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_3', null , ['placeholder' => 'Career Inquiry Admin Email', 'class' => 'form-control', 'id' => 'inputCareerInquires']) !!}
                                    <span class="text-danger">{{ $errors->first('input_3') }}</span>
                                </div>
                            </div>

                        </div>
                        <div id="arabic_content" class="tab-pane" style="display:none;">
                            <div class="form-group row">
                                {!! Form::label('inputBusinessInquires', 'Business Inquiry Admin Email', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_1_ar', null , ['placeholder' => 'Business Inquiry Admin Email', 'class' => 'form-control ar_input', 'id' => 'inputBusinessInquires']) !!}
                                    <span class="text-danger">{{ $errors->first('input_1_ar') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputGeneralInquires', 'General Inquiry Admin Email', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_2_ar', null , ['placeholder' => 'General Inquiry Admin Email', 'class' => 'form-control ar_input', 'id' => 'inputGeneralInquires']) !!}
                                    <span class="text-danger">{{ $errors->first('input_2_ar') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('inputCareerInquires', 'Career Inquiry Admin Email', ['class' => 'col-sm-3 col-form-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('input_3_ar', null , ['placeholder' => 'Career Inquiry Admin Email', 'class' => 'form-control ar_input', 'id' => 'inputCareerInquires']) !!}
                                    <span class="text-danger">{{ $errors->first('input_3_ar') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-sm-3 col-sm-9">
                            {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.card-body -->
                </div>
                <!-- /.card -->
                @endif
            </section>
    </main>
</x-master-admin-layout>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="{{ asset('js/jquery.toaster.js') }}" defer></script>
<script>
    $('.nav-tabs a').on('click', function(e) {
        e.preventDefault();
        if ($(this).attr('href') == '#english_content') {
            $('#arabic_content').hide();
            $('#english_content').show();
        } else {
            $('#english_content').hide();
            $('#arabic_content').show();
        }
        $(this).tab('show');
    });
    $('input[type=file]').change(function(event) {
        const target = event.target;
        if ($(this).attr('name') == 'video_mp4') {
            var imgPath = $(this).val(); //fileUpload.value;
            var regex = new RegExp("^.*\.(mp4)$");
            var file_size = target.files[0].size;
            if (regex.test(imgPath.toLowerCase()) == '') {

                $(this).val('');
                $.toaster({
                    priority: 'danger',
                    title: "Error",
                    message: 'Please upload MP4 video',
                    timeout: 5000,
                });
            }
        } else if ($(this).attr('name') == 'video_webm') {
            var imgPath = $(this).val(); //fileUpload.value;
            var regex = new RegExp("^.*\.(webm)$");
            var file_size = target.files[0].size;
            if (regex.test(imgPath.toLowerCase()) == '') {

                $(this).val('');
                $.toaster({
                    priority: 'danger',
                    title: "Error",
                    message: 'Please upload WEBM video',
                    timeout: 5000,
                });
            }
        }
        if (target.files && target.files[0]) {

            /*Maximum allowed size in bytes
              20MB Example // 20,971,520
              Change first operand(multiplier) for your needs*/
            const maxAllowedSize = 20 * 1024 * 1024;
            if (target.files[0].size > maxAllowedSize) {
                $(this).val('');
                $.toaster({
                    priority: 'danger',
                    title: "Error",
                    message: 'Uploaded file too large. Acceptable file size is 20MB',
                    timeout: 5000,
                });
            }
        }
    });
</script>