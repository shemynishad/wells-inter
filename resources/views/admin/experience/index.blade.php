<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/component/{{ $experiences->tt_content_id }}/edit">Experices</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Experience List</h3>
                    <div class="card-tools">
                    <a href="/admin/{{ $experiences->tt_content_id }}/experience/create"> 
                        <i class="fas fa-plus"></i>
                        Add Experience 
                    </a>
                    </div>
                </div>
            </div>
            @foreach($experiences as $experience)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"></h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-sm-3 text-center"> 
                                <h2 class="exp_headline"> {{ $experience->headline }} </h2>
                                <span class="exp_divider"> ................................. </span>
                                <h3 class="exp_sub_headline"> {!! nl2br($experience->sub_headline) !!} </h3>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="">
                            <a href="/admin/{{ $experience->tt_content_id }}/experience/{{ $experience->id }}/edit" class="btn btn-primary">
                                <i class="fas fa-plus"></i> Edit
                            </a>
                            <a data-toggle="modal" data-target="#deleteModal_{{$experience->id}}" class="btn btn-primary">
                                <i class="fas fa-trash"></i> Delete
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="deleteModal_{{$experience->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel_{{$experience->id}}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="deleteModalLabel_{{$experience->id}}">Delete Experience Slide</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure, you wants to delete ...
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::open(['method' => 'delete', 'route' => ['admin.experience.destroy', $experience->tt_content_id, $experience->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Delete</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-footer-->
                </div>
                <!-- /.card -->
            @endforeach
        </section>
    </main>
</x-master-admin-layout>
