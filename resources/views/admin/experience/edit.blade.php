<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/component/{{ $experience->tt_content_id }}/edit">Experience</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Experience Edit</h3>
                </div>
                <div class="card-body">
                    {!! Form::model($experience, ['method' => 'put', 'route' => ['admin.experience.update', $experience->tt_content_id, $experience->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                    <ul class="nav nav-tabs font-bold">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#english_content">English &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#arabic_content">Arabic &nbsp;&nbsp;&nbsp;&nbsp;</a>
                        </li>
                    </ul>
                    <br />
                    <div id="english_content" class="tab-pane active">
                        <div class="form-group row">
                            {!! Form::label('inputHeadline', 'Main Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('headline', null , ['placeholder' => 'Main Headline', 'class' => 'form-control', 'id' => 'inputHeadline']) !!}
                                <span class="text-danger">{{ $errors->first('headline') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputSubHeadline', 'Sub Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('sub_headline', null , ['placeholder' => 'Sub Headline', 'class' => 'form-control', 'id' => 'inputSubHeadline']) !!}
                                <span class="text-danger">{{ $errors->first('sub_headline') }}</span>
                            </div>
                        </div>
                    </div>
                    <div id="arabic_content" class="tab-pane" style="display: none;">
                        <div class="form-group row">
                            {!! Form::label('inputHeadline', 'Main Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('headline_ar', null , ['placeholder' => 'Main Headline', 'class' => 'form-control ar_input', 'id' => 'inputHeadline']) !!}
                                <span class="text-danger">{{ $errors->first('headline_ar') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputSubHeadline', 'Sub Headline', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('sub_headline_ar', null , ['placeholder' => 'Sub Headline', 'class' => 'form-control ar_input', 'id' => 'inputSubHeadline']) !!}
                                <span class="text-danger">{{ $errors->first('sub_headline_ar') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="offset-sm-3 col-sm-9">
                            {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
    </main>
</x-master-admin-layout>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
    $('.nav-tabs a').on('click', function(e) {
        e.preventDefault();
        if ($(this).attr('href') == '#english_content') {
            $('#arabic_content').hide();
            $('#english_content').show();
        } else {
            $('#english_content').hide();
            $('#arabic_content').show();
        }
        $(this).tab('show');
    });
</script>