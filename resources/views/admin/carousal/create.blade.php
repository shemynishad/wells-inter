<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/component/{{ $tt_content_id }}/edit">Carousal</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Carousal Create</h3>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'post', 'route' => ['admin.carousal.store', $tt_content_id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id', $tt_content_id) !!}
                        <div class="form-group row">
                            {!! Form::label('inputCarousaltitle', 'Title(EN)', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('title', null , ['placeholder' => 'Title', 'class' => 'form-control', 'id' => 'inputCarousaltitle']) !!}
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputCarousaltitle', 'Title(AR)', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('title_en', null , ['placeholder' => 'Title', 'class' => 'form-control ar_input', 'id' => 'inputCarousaltitle']) !!}
                                <span class="text-danger">{{ $errors->first('title_en') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputCarousalOrder', 'Order', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('order', null , ['placeholder' => 'Order', 'class' => 'form-control', 'id' => 'inputCarousalOrder']) !!}
                                <span class="text-danger">{{ $errors->first('order') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputText', 'Text(EN)', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('description', null , ['placeholder' => 'Text', 'class' => 'form-control', 'id' => 'inputText']) !!}
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputText', 'Text(AR)', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('description_ar', null , ['placeholder' => 'Text', 'class' => 'form-control ar_input', 'id' => 'inputText']) !!}
                                <span class="text-danger">{{ $errors->first('description_ar') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputThumbnail', 'Thumbnail', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                <div class="input-group col-sm-12">
                                    <div class="custom-file">
                                        {!! Form::file('image_1',['class' => 'custom-file-input', 'id' => 'inputThumbnail']) !!}
                                        {!! Form::label('inputThumbnail', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                    </div>
                                </div>
                                <span class="text-danger">{{ $errors->first('image_1') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
    </main>
</x-master-admin-layout>
