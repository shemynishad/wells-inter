<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/component/{{ $carousals->tt_content_id }}/edit">Carousal</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Carousal List</h3>
                    <div class="card-tools">
                    <a href="/admin/{{ $carousals->tt_content_id }}/carousal/create"> 
                        <i class="fas fa-plus"></i>
                        Add Carousal 
                    </a>
                    </div>
                </div>
            </div>
            @foreach($carousals as $carousal)
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$carousal->order }} - {{ $carousal->title }}</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-md-8 order-md-2">
                                <div class="section-inner-txt order-md-2">
                                    {{ $carousal->description }}
                                </div>
                            </div>
                            <div class="col-md-4 order-md-1">
                                <div class="section-inner-img" style="height: 450px;">
                                    <img src="{{ asset('images/carousal/')}}/{{ $carousal->thumbnail }}" style="height: 100%;">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="">
                            <a href="/admin/{{ $carousal->tt_content_id }}/carousal/{{ $carousal->id }}/edit" class="btn btn-primary">
                                <i class="fas fa-plus"></i> Edit
                            </a>
                            <a data-toggle="modal" data-target="#deleteModal_{{$carousal->id}}" class="btn btn-primary">
                                <i class="fas fa-trash"></i> Delete
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="deleteModal_{{$carousal->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel_{{$carousal->id}}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="deleteModalLabel_{{$carousal->id}}">Delete Carousal Slide</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure, you wants to delete ...
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::open(['method' => 'delete', 'route' => ['admin.carousal.destroy', $carousal->tt_content_id, $carousal->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Delete</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-footer-->
                </div>
                <!-- /.card -->
            @endforeach
        </section>
    </main>
</x-master-admin-layout>
