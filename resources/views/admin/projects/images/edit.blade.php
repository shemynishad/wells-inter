<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/pages/8">Projects Page</a></li>
                        <li class="breadcrumb-item"><a href="/admin/{{ $project_image->tt_content_id }}/project">Projects</a></li>
                        <li class="breadcrumb-item active">Project ImageCreate</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif 
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit ({{$project_image->project->project_name}})</h3>
                </div>
                <div class="card-body">
                    {!! Form::model($project_image, ['method' => 'put', 'route' => ['admin.project.project-image.update', $project_image->tt_content_id, $project_image->project_id, $project_image->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id', $project_image->tt_content_id) !!}
                        {!! Form::hidden('project_id', $project_image->project_id) !!}
                        <div class="form-group row">
                            {!! Form::label('inputProjectImageCaption', 'Project Image Caption', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('image_caption', null , ['placeholder' => 'Project Image Caption', 'class' => 'form-control', 'id' => 'inputProjectImageCaption']) !!}
                                <span class="text-danger">{{ $errors->first('image_caption') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputOrder', 'Order', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('order', null , ['placeholder' => 'Order', 'class' => 'form-control', 'id' => 'inputOrder']) !!}
                                <span class="text-danger">{{ $errors->first('order') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputProjectImageClass', 'Project Image Slot', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('image_class', [
                                                                    "49" => 'None', 
                                                                    "1" => 'Slot 1', 
                                                                    "2" => 'Slot 2', 
                                                                    "3" => 'Slot 3', 
                                                                    "4" => 'Slot 4', 
                                                                    "5" => 'Slot 5',
                                                                    "6" => 'Slot 6',
                                                                    "7" => 'Slot 7',
                                                                    "8" => 'Slot 8',
                                                                    "9" => 'Slot 9',
                                                                    "10" => 'Slot 10',
                                                                    "11" => 'Slot 11',
                                                                    "12" => 'Slot 12',
                                                                    "13" => 'Slot 13',
                                                                    "14" => 'Slot 14',
                                                                    "15" => 'Slot 15',
                                                                    "16" => 'Slot 16',
                                                                    "17" => 'Slot 17',
                                                                    "18" => 'Slot 18',
                                                                    "19" => 'Slot 19',
                                                                    "20" => 'Slot 20',
                                                                    "21" => 'Slot 21',
                                                                    "22" => 'Slot 22',
                                                                    "23" => 'Slot 23',
                                                                    "24" => 'Slot 24',
                                                                    "25" => 'Slot 25',
                                                                    "26" => 'Slot 26',
                                                                    "27" => 'Slot 27',
                                                                    "28" => 'Slot 28',
                                                                    "29" => 'Slot 29',
                                                                    "30" => 'Slot 30',
                                                                    "31" => 'Slot 31',
                                                                    "32" => 'Slot 32',
                                                                    "33" => 'Slot 33',
                                                                    "34" => 'Slot 34',
                                                                    "35" => 'Slot 35',
                                                                    "36" => 'Slot 36',
                                                                    "37" => 'Slot 37',
                                                                    "38" => 'Slot 38',
                                                                    "39" => 'Slot 39',
                                                                    "40" => 'Slot 40',
                                                                    "41" => 'Slot 41',
                                                                    "42" => 'Slot 42',
                                                                    "43" => 'Slot 43',
                                                                    "44" => 'Slot 44',
                                                                    "45" => 'Slot 45',
                                                                    "46" => 'Slot 46',
                                                                    "47" => 'Slot 47',
                                                                    "48" => 'Slot 48'
                                                                ], null, ['class'=>'form-control select2','placeholder'=>'Select Project Image Slot', 'id'=>'inputProjectImageClass']) !!}
                                <span class="text-danger">{{ $errors->first('image_class') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputProjectImage', 'Project Image', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                <div class="input-group col-sm-12">
                                    <div class="custom-file">
                                        {!! Form::file('image',['class' => 'custom-file-input', 'id' => 'inputProjectImage']) !!}
                                        {!! Form::label('inputProjectImage', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                    </div>
                                </div>
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                                <span class="p-3">-- OR -- </span>
                                <a class="mt-2" data-toggle="modal" data-target="#deleteGalleryImageModal">
                                    <i class="fas fa-trash"></i>
                                    Remove Gallery Image
                                </a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
        <!-- Modal -->
        <div class="modal fade" id="deleteGalleryImageModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Delete Gallery Image</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure, you wants to delete ...
                    </div>
                    <div class="modal-footer">
                        {!! Form::open(['method' => 'delete', 'route' => ['admin.project.project-image.destroy', $project_image->tt_content_id, $project_image->project_id, $project_image->id], 'class' => 'form-horizontal']) !!}
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Delete</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </main>
</x-master-admin-layout>
