<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/pages/8">Projects Page</a></li>
                        <li class="breadcrumb-item active">Project List</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Project List</h3>
                    <div class="card-tools">
                    <a href="/admin/{{ $projects->tt_content_id }}/project/create"> 
                        <i class="fas fa-plus"></i>
                        Add Project
                    </a>
                    </div>
                </div>
            </div>
            <div class="accordion">
                @foreach($projects as $project)
                    <div class="card">
                        <div class="card-header" id="header-text-wrapper">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#project-{{ $project->id }}" aria-expanded="true" aria-controls="home_header_text_element" style="box-shadow: none;">
                                    {{ $project->project_name }}
                                </button>
                            </h2>
                        </div>
                        <div id="project-{{ $project->id }}" class="collapse">
                            <div class="card-body">
                                <h3> Project Slots </h3>
                                <div class="row">
                                    @foreach($project->project_slot as $slot_image)
                                        <div class="col-sm-6 col-md-2 text-center d-flex align-items-center justify-content-center flex-column">
                                            <img src="/images/projects_slots/{{$slot_image->thumbnail}}" width="203" height="270" />
                                            <span> Slot# {{ $slot_image->slot_id }} </span>
                                            <a class="mt-2" href="/admin/{{ $project->tt_content_id }}/project/{{ $project->id }}/slot-image/{{ $slot_image->id }}/edit">
                                                <i class="fas fa-edit"></i>
                                                Edit Scrap Book Image
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                                <h3 class="mt-2"> Project Galery </h3>
                                <div class="row">
                                    @foreach($project->project as $image)
                                        <div class="col-sm-6 col-md-2 text-center d-flex align-items-center justify-content-center flex-column">
                                            <img src="/images/projects_gallery/{{$image->image}}" width="203" height="270" />
                                            <span> {{ $image->image_caption }} </span>
                                            @if($image->image_class == 49)
                                                <span> (No Slot) </span>
                                            @else 
                                                <span> (Slot# {{ $image->image_class }}) </span>
                                            @endif
                                            <a class="mt-2" href="/admin/{{ $project->tt_content_id }}/project/{{ $project->id }}/project-image/{{ $image->id }}/edit" >
                                                <i class="fas fa-edit"></i>
                                                Edit Gallery Image
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- Card Body ends -->
                            <div class="card-footer">
                                <div class="">
                                    <a href="/admin/{{ $project->tt_content_id }}/project/{{ $project->id }}/slot-image/create" class="btn btn-primary">
                                        <i class="fas fa-plus"></i> Add Project Slot Image
                                    </a>
                                    <a href="/admin/{{ $project->tt_content_id }}/project/{{ $project->id }}/project-image/create" class="btn btn-primary">
                                        <i class="fas fa-plus"></i> Add Project Gallery Image
                                    </a>
                                    <a href="/admin/{{ $project->tt_content_id }}/project/{{ $project->id }}/edit" class="btn btn-primary">
                                        <i class="fas fa-plus"></i> Edit
                                    </a>
                                    <a data-toggle="modal" data-target="#deleteGalleryImageModal_{{$project->id}}" class="btn btn-primary">
                                        <i class="fas fa-trash"></i> Delete
                                    </a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="deleteGalleryImageModal_{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel_{{$project->id}}" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel_{{$project->id}}">Delete Project</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure, you wants to delete ...
                                                </div>
                                                <div class="modal-footer">
                                                    {!! Form::open(['method' => 'delete', 'route' => ['admin.project.destroy', $project->tt_content_id, $project->id], 'class' => 'form-horizontal']) !!}
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Delete</button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-footer-->
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </main>
</x-master-admin-layout>
