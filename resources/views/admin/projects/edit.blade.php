<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/{{ $project->tt_content_id }}/project">Projects</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Project Edit</h3>
                </div>
                <div class="card-body">
                    {!! Form::model($project, ['method' => 'put', 'route' => ['admin.project.update', $project->tt_content_id, $project->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        <div class="form-group row">
                            {!! Form::label('inputProjectName', 'Project Name', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('project_name', null , ['placeholder' => 'Project Name', 'class' => 'form-control', 'id' => 'inputProjectName']) !!}
                                <span class="text-danger">{{ $errors->first('project_name') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
    </main>
</x-master-admin-layout>
