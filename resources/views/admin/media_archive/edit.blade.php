<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/component/{{ $media_archive->tt_content_id }}/edit">Media Archive</a></li>
                        <li class="breadcrumb-item active">Archive Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Media Archive Edit</h3>
                    <div class="card-tools">
                        <a href="/admin/{{ $media_archive->tt_content_id }}/media-archive/{{$media_archive->id}}/gallery/create">
                            <i class="fas fa-plus"></i>
                            Add Gallery Image
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    {!! Form::model($media_archive, ['method' => 'put', 'route' => ['admin.media-archive.update', $media_archive->tt_content_id, $media_archive->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                    <div class="form-group row">
                        {!! Form::label('inputCategory', 'Media Archive Category', ['class' => 'col-sm-3 col-form-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::select('media_archive_category_id', $media_archive->categories, null, ['class'=>'form-control','placeholder'=>'Select Media Archive Category', 'id'=>'inputCategory']) !!}
                            <span class="text-danger">{{ $errors->first('media_archive_category_id') }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('inputCarousalOrder', 'Order', ['class' => 'col-sm-3 col-form-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('order', null , ['placeholder' => 'Order', 'class' => 'form-control', 'id' => 'inputCarousalOrder']) !!}
                            <span class="text-danger">{{ $errors->first('order') }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('inputLink', 'Link', ['class' => 'col-sm-3 col-form-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('link', null , ['placeholder' => 'Link', 'class' => 'form-control', 'id' => 'inputLink']) !!}
                            <span class="text-danger">{{ $errors->first('link') }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('inputVideo', 'Video ID', ['class' => 'col-sm-3 col-form-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('video_url', null , ['placeholder' => 'Video ID', 'class' => 'form-control', 'id' => 'inputVideo']) !!}
                            <span class="text-danger">{{ $errors->first('video_url') }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('inputThumbnail', 'Thumbnail', ['class' => 'col-sm-3 col-form-label']) !!}
                        <div class="col-sm-9">
                            <div class="input-group col-sm-12">
                                <div class="custom-file">
                                    {!! Form::file('thumbnail',['class' => 'custom-file-input', 'id' => 'inputThumbnail']) !!}
                                    {!! Form::label('inputThumbnail', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                </div>
                            </div>
                            <span class="text-danger">{{ $errors->first('thumbnail') }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-sm-3 col-sm-9">
                            {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <h2> Gallery Items </h2>
                    <hr>
                    <div class="row">
                        @foreach($media_archive->media_archive_gallery as $gallery)
                        <div class="col-sm-12 col-md-6 col-lg-2 mt-3 d-flex flex-column justify-content-center align-items-center">
                            <img src="/images/media_archive/{{$gallery->image}}" />
                            <a class="mt-2" href="/admin/{{ $media_archive->tt_content_id }}/media-archive/{{ $media_archive->id }}/gallery/{{ $gallery->id }}/edit">
                                <i class="fas fa-edit"></i>
                                Edit Gallery Image
                            </a>
                            <a class="mt-2" data-toggle="modal" data-target="#deleteModal_{{$gallery->id}}">
                                <i class="fas fa-trash"></i>
                                Remove Gallery Image
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="deleteModal_{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel_{{$gallery->id}}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="deleteModalLabel_{{$gallery->id}}">Delete Gallery Image</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure, you wants to delete ...
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::open(['method' => 'delete', 'route' => ['admin.gallery.destroy', $media_archive->tt_content_id, $media_archive->id, $gallery->id], 'class' => 'form-horizontal']) !!}
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-master-admin-layout>