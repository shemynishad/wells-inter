<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/component/{{ $tt_content_id }}/edit">Brands</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Brands Create</h3>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'post', 'route' => ['admin.brands.store', $tt_content_id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id', $tt_content_id) !!}
                        <div class="form-group row">
                            {!! Form::label('inputBrandName', 'Brand Name(EN)', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('brand_name', null , ['placeholder' => 'Brand Name', 'class' => 'form-control', 'id' => 'inputBrandName']) !!}
                                <span class="text-danger">{{ $errors->first('brand_name') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputBrandName', 'Brand Name(AR)', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('brand_name_ar', null , ['placeholder' => 'Brand Name', 'class' => 'form-control ar_input', 'id' => 'inputBrandName']) !!}
                                <span class="text-danger">{{ $errors->first('brand_name_ar') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputOrder', 'Order', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('order', null , ['placeholder' => 'Order', 'class' => 'form-control', 'id' => 'inputOrder']) !!}
                                <span class="text-danger">{{ $errors->first('order') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputBrandImage', 'Brand Image', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                <div class="input-group col-sm-12">
                                    <div class="custom-file">
                                        {!! Form::file('brand_image',['class' => 'custom-file-input', 'id' => 'inputBrandImage']) !!}
                                        {!! Form::label('inputBrandImage', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                    </div>
                                </div>
                                <span class="text-danger">{{ $errors->first('brand_image') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            @endif
        </section>
    </main>
</x-master-admin-layout>
