<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/social-media/1/edit">Social Media</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Social Media Edit</h3>
                </div>
                <div class="card-body">
                    {!! Form::model($social_media, ['method' => 'put', 'route' => ['admin.social-media.update',  $social_media->id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        <div class="form-group row">
                            {!! Form::label('inputInstagram', 'Instagram Link', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('instagram_link', null , ['placeholder' => 'Instagram Link', 'class' => 'form-control', 'id' => 'inputInstagram']) !!}
                                <span class="text-danger">{{ $errors->first('instagram_link') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputPinterest', 'Pinterest Link', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pinterest_link', null , ['placeholder' => 'Pinterest Link', 'class' => 'form-control', 'id' => 'inputPinterest']) !!}
                                <span class="text-danger">{{ $errors->first('pinterest_link') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputLN', 'LinkedIn Link', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('linkedin_link', null , ['placeholder' => 'LinkedIn Link', 'class' => 'form-control', 'id' => 'inputLinkedin']) !!}
                                <span class="text-danger">{{ $errors->first('linkedin_link') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
    </main>
</x-master-admin-layout>
