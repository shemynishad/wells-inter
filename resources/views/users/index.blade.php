<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid px-3">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Users') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">User's</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        @if(session()->has('message'))
            <div class="alert alert-primary alert-dismissible fade show mx-2" role="alert">
                <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <section class="content">
            <div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">User's List</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="dataTables" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th class="no-sort">Edit | Delete</th>
                                            </tr>
                                        </thead>
                                        <thead class="filter">
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                    <td> {{ $loop->iteration }} </td>
                                                    <td> {{ $user->name }} </td>
                                                    <td> {{ $user->email }} </td>
                                                    <td>
                                                        <a class="ml-3" href="users/{{ $user->id }}/edit">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        <a class="ml-3" data-toggle="modal" data-target="#deleteModal_{{$user->id}}">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="deleteModal_{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel_{{$user->id}}" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="deleteModalLabel_{{$user->id}}">Delete User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Are you sure, you wants to delete ...
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        {!! Form::open(['method' => 'delete', 'route' => ['users.destroy', $user->id], 'class' => 'form-horizontal']) !!}
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-primary">Delete</button>
                                                                        {!! Form::close() !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
    </main>
</x-master-admin-layout>
