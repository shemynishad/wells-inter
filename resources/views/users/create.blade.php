<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Users') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Users</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">User Registration</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'post', 'route' => ['users.store'], 'class' => 'form-horizontal']) !!}
                        <div class="form-group row">
                            {!! Form::label('inputName', 'Name', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name', null , ['placeholder' => 'Name', 'class' => 'form-control', 'id' => 'inputName']) !!}
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputEmail', 'E-Mail', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('email', null , ['placeholder' => 'E-Mail', 'class' => 'form-control', 'id' => 'inputEmail']) !!}
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputPassword', 'Enter Password', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::password('password', ['placeholder' => 'Enter Password', 'class' => 'form-control', 'id' => 'inputPassword']) !!}
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputConfirmPassword', 'Password Confirmation', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::password('password_confirmation', ['placeholder' => 'Password Confirmation', 'class' => 'form-control', 'id' => 'inputConfirmPassword']) !!}
                                <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </section>
    </main>
</x-master-admin-layout>
