<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Contact | Wells International</title>
        <meta name="author" content="Wells International" />
        <meta name="description" content="Contact" />
        <meta name="keywords" content="wells international contact" />
        <meta name="Resource-type" content="Document" />
        <meta name="format-detection" content="telephone=no">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="{{ asset('images') }}/logo-nav.png" />
        <link rel="apple-touch-icon" href="{{ asset('images') }}/logo-nav.png" />
        <link rel="apple-touch-icon-precomposed" href="{{ asset('images') }}/logo-nav.png" />
        <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.fullpage.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css" />
        <link rel="stylesheet" type="text/css" href="css/aos.css" />
        <link rel="stylesheet" type="text/css" href="css/custom_style.css" />
    </head>
    <body class="contact_us_page">
        <!--<div class="se-pre-con"></div>--> 
        <a id="top"></a>
        <x-header />
        <div id="fullpage">
            <div class="section" id="sectioncontactus">
                @foreach($tt_contents as $content)
                    @if($content->component_id == 13)
                        <x-contactVideoComponent :id="$content->id" />
                    @elseif($content->component_id == 4)
                        <x-footerComponent :id="$content->id" />
                    @endif
                @endforeach
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> 
        <script src="js/bootstrap.min.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
        <script type="text/javascript" src="js/scrolloverflow.js"></script> 
        <script type="text/javascript" src="js/aos.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script>
            function myFunction() { 
            var x = document.getElementById("myVideo").autoplay;
            } 
            AOS.init({
                duration: 1000,
                once: true,
                delay:1000,
            })
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-5D91DFSKVS"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'G-5D91DFSKVS');
        </script>
    </body>
</html>