<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Wells International - Global Hospitalty Design Consultants</title>
        <meta name="author" content="Wells International - Global Hospitalty Design Consultants" />
        <meta name="description" content="'We Design Stories' Our Interior Design expertise is the creation of inventive ideation and powerful storytelling." />
        <meta name="keywords" content="wells international home" />
        <meta name="Resource-type" content="Document" />
        <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="{{ asset('images') }}/logo-nav.png" />
        <link rel="apple-touch-icon" href="{{ asset('images') }}/logo-nav.png" />
        <link rel="apple-touch-icon-precomposed" href="{{ asset('images') }}/logo-nav.png" />
        <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.fullpage.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css" />
    </head>
    <body>

        <x-header />
        <div id="fullpage">
            @foreach($tt_contents as $content)
                @if($content->component_id == 1)
                    <x-homePageVideoComponent :id="$content->id" />
                @endif
            @endforeach
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/custom.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
        <script type="text/javascript" src="js/scrolloverflow.js"></script>
        <script type="text/javascript" src="js/jquery.fullpage.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#fullpage').fullpage({
                    verticalCentered: true,
                    sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE']
                });
                $('body').on('click','.mute_btn', function() {
                    if($(this).hasClass('active')) {
                        $(this).removeClass('active');
                    } else {
                        $(this).addClass('active');
                    }
                });
            });
        </script>
        <script>
        
            // function myFunction() {
            //     var x = document.getElementById("myVideo").autoplay;
            // }
            var vid = document.getElementById("myVideo");
            function enableMute() {
                var video=document.getElementById("myVideo");
                video.muted = !video.muted;
            }
            jQuery(document).ready(function ($) {
                document.getElementById("myVideo").controls = false;
                // Get the video element with id="myVideo"
                var vid = document.getElementById("myVideo");
                // Assign an ontimeupdate event to the video element, and execute a function if the current playback position has changed
                vid.ontimeupdate = function () {
                    myFunction()
                };
                function myFunction() {
                    // Display the current position of the video in a p element with id="demo"
                    var hello = Math.round(vid.currentTime);
                    if (Math.round(vid.currentTime) == 4) {
                        $('#homeid1').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 5) {
                        $('#homeid2').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 9) {
                        $('#homeid1').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 9) {
                        $('#homeid2').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 14) {
                        $('#homeid3').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 18) {
                        $('#homeid3').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 20) {
                        $('#homeid4').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 22) {
                        $('#homeid4').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 23) {
                        $('#homeid5').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 25) {
                        $('#homeid5').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 26) {
                        $('#homeid6').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 28) {
                        $('#homeid6').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 29) {
                        $('#homeid7').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 31) {
                        $('#homeid7').fadeOut(1000);
                    }
                    if (Math.round(vid.currentTime) == 33) {
                        $('#homeid8').fadeIn(1000);
                    }
                    if (Math.round(vid.currentTime) == 44) {
                        $('#homeid8').fadeOut(1000);
                    }
                }
            });
            $("#embed_wistia_video").trigger("click");
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-5D91DFSKVS"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'G-5D91DFSKVS');
        </script>
    </body>
</html>
