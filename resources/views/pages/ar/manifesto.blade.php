<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Manifesto | Wells International</title>
        <meta name="author" content="wells" />
        <meta name="description" content="wells dev" />
        <meta name="keywords" content="wells" />
        <meta name="Resource-type" content="Document" />
        <link rel="icon" href="{{ asset('images') }}/favicon.ico" type="image/x-icon"/>
        <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css') }}/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('css') }}/bootstrap-rtl/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('css') }}/jquery.fullpage.css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css') }}/style.css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css') }}/custom_style.css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css') }}/arabic_style.css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('fonts') }}/fonts.css" />
        <style>
            #sectionmenifesto{background-color:transparent !important;}
            .fp-controlArrow.fp-next , .fp-controlArrow.fp-prev{
                border:none;
                background-image: url({{ asset('images') }}/right-arrow_big.svg);
                background-size: 17px;
            }
            @media (min-width: 320px) and (max-width: 480px) {
                .manifesto_intro{top:64%;}
                .manifesto_name{top:92% !important;}
                .manipopup{top:62% !important;}
                .mani_last .manifesto_intro {max-height: 520px;}
                .close{font-weight:0;text-shadow: none !important;}
                .justinbutton{left: 46% !important;}
            }
            div.gfg { 
                position: relative; 
                overflow-x: hidden; 
                overflow-y: scroll; 
                -webkit-overflow-scrolling: touch; 
                max-height: 300px;
                margin:5px; 
                padding:5px; 
                background-color: green; 
                width: 1000px;
                text-align:justify; 
            }
        </style>
    </head>

    <body class="hotels_main1">
        <a id="top"></a>

        <x-arabicHeaderComponent />
        <div id="fullpageSection">
            @foreach($tt_contents as $content)
                @if($content->component_id == 11)
                    <x-manifestoComponent :id="$content->id" />
                @endif
            @endforeach
        </div>
        

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="{{ asset('js') }}/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>  
        <script type="text/javascript" src="{{ asset('js') }}/scrolloverflow.js"></script> 
        <script type="text/javascript" src="{{ asset('js') }}/jquery.fullpage.js"></script> 
        <script type="text/javascript" src="{{ asset('js') }}/custom.js"></script> 
        <script type="text/javascript">
            $(document).ready(function () {
                $('#fullpage').fullpage({
                    verticalCentered: true,
                    sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE']
                });
            });

            jQuery(document).ready(function ($) {
                document.getElementById("myVideo").controls = false;
                // Get the video element with id="myVideo"
                var vid = document.getElementById("myVideo");
                // Assign an ontimeupdate event to the video element, and execute a function if the current playback position has changed
                vid.ontimeupdate = function () {
                    myFunction()
                };
                setTimeout(function(){
                    $('#sectionmenifesto2').fadeIn(3000);
                },9000);
                setTimeout(function(){
                    $('#sectionmenifesto').hide();
                },9200);
                setTimeout(function(){
                    $('.justinbutton').fadeIn(1000);
                },10800);
                function myFunction() {
                    // Display the current position of the video in a p element with id="demo"
                    if (Math.round(vid.currentTime) == 2) {
                        $('#approach011').fadeIn(2000);
                    }
                    if (Math.round(vid.currentTime) == 6) {
                    
                        $('#approach011').fadeOut(2000);
                    }
                    if (Math.round(vid.currentTime) == 7) {
                        $('#approach023').fadeIn(2000);
                    }
                    if (Math.round(vid.currentTime) == 10) {
                        $('#approach023').hide();
                    }
                }
            });
            $('#sectionmenifesto7').hide();
            $(".close").click(function(){
                $('.fp-prev').show();
                $('.fp-next').show();
                $('#sectionmenifesto7').fadeOut(500);
                $('#sectionmenifesto3').fadeIn(500);
                $('.justinbutton').show();
            });
            $(".justinbutton").click(function(){
                $('.fp-prev').hide();
                $('.fp-next').hide();
                $('.justinbutton').hide();
                $('#sectionmenifesto7').fadeIn(500);
            });
        </script> 
        <script>
            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 1000;
                document.documentElement.scrollTop = 1000;
            }
            function myFunction() { 
                var x = document.getElementById("myVideo").autoplay;
            } 
            var that = this;
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-5D91DFSKVS"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'G-5D91DFSKVS');
        </script>
    </body>
</html>