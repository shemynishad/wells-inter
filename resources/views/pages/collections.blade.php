<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Collections | Wells International</title>
        <meta name="author" content="Wells International" />
        <meta name="description" content="Collections" />
        <meta name="keywords" content="wells international collections" />
        <meta name="Resource-type" content="Document" />
        <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="{{ asset('images') }}/logo-nav.png" />
        <link rel="apple-touch-icon" href="{{ asset('images') }}/logo-nav.png" />
        <link rel="apple-touch-icon-precomposed" href="{{ asset('images') }}/logo-nav.png" />
        <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.fullpage.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css" />
        <link rel="stylesheet" type="text/css" href="https://unpkg.com/aos@2.3.0/dist/aos.css" />
        <style>
            #ascrail2000 {
                z-index: 9999 !important;
            }
            .slick-prev, .slick-next{
                border:none;
            }
            .slick-next::before, .slick-prev::before{
                background-image: url(../images/right-arrow.svg);
            }
            .slick-prev:hover, .slick-prev:focus, .slick-next:hover, .slick-next:focus {
                background-color: transparent;
            }
        </style>
    </head>
    <body class="hotels_main">
        <!--<div class="se-pre-con"></div>--> 
        <a id="top"></a>

        <x-header />
        <div id="fullpage">
            @foreach($tt_contents as $content)
                @if($content->component_id == 2)
                    <x-secondaryPageVideoWithScrollerAndSlides :id="$content->id" />
                @elseif($content->component_id == 3)
                    <x-carouselComponent :id="$content->id" />
                @elseif($content->component_id == 4)
                    <x-footerComponent :id="$content->id" />
                @endif
            @endforeach
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
        <script src="js/jquery-migrate-1.2.1.min.js"></script> 
        <script src="js/slick.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> 
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> 
        <script type="text/javascript" src="js/custom.js"></script> 
        <script>
            $(document).ready(function () {
                $('.slick-carousel').slick({
                    speed: 500,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    // autoplay: true,
                    autoplaySpeed: 2000,
                    content: true,
                    dots: false,
                    arrows: true,
                    centerMode: false,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                infinite: true,
                                dots: true
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                arrows: false,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                        // You can unslick at a given breakpoint now by adding:
                        // settings: "unslick"
                        // instead of a settings object
                    ]
                });
                $(window).on('resize', function () {
                    $('.slick-carousel').slick('resize');
                });
            });
            $(window).scroll(function () {
                if ($(this).scrollTop() > 300) {
                    $('.hotels_main').addClass('projects_main');
                } else {
                    $('.hotels_main').removeClass('projects_main');
                }
            });
            $(document).ready(function () {
                $('#myBtn').click(function () {
                    var bheight = $(window).height();
                    $("html, body").animate({scrollTop: bheight}, 600);
                    return false;
                });
                $('#scroll_bottm').click(function () {
                    $("html, body").animate({scrollTop: 0}, 600);
                    return false;
                });
            });
        </script> 
        <script>
            function myFunction() {
                var x = document.getElementById("myVideo").autoplay;
            }
        </script> 
        <script type="text/javascript" src="https://unpkg.com/aos@2.3.0/dist/aos.js"></script>
        <script src="js/jquery.easing.min.js"></script> 
        <script src="js/jquery.mousewheel.min.js"></script> 
        <script src="js/viewScroller.js"></script>
        <link rel="stylesheet" type="text/css" href="js/viewScroller.css">
        <script>
            $(document).ready(function() {
                // Sets viewScroller
                $('.mainbag').viewScroller({
                    useScrollbar: true,
                    changeWhenAnim: false
                });
                AOS.init({
                    duration: 1000,
                })
            });
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-5D91DFSKVS"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'G-5D91DFSKVS');
        </script>
    </body>
</html>