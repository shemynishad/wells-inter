<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Projects | Wells International</title>
        <meta name="author" content="Wells International" />
        <meta name="description" content="Projects" />
        <meta name="keywords" content="wells international projects" />
        <meta name="Resource-type" content="Document" />
        <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="{{ asset('images') }}/logo-nav.png" />
        <link rel="apple-touch-icon" href="{{ asset('images') }}/logo-nav.png" />
        <link rel="apple-touch-icon-precomposed" href="{{ asset('images') }}/logo-nav.png" />
        <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@300;400;500;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.fullpage.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/custom_style.css" />
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css" />
        <link rel="stylesheet" type="text/css" href="https://unpkg.com/aos@2.3.0/dist/aos.css" />
        <!-- <link rel="stylesheet" href="css/magnific-popup.css"> -->
        <style>
            #ascrail2000 {
                z-index: 9999 !important;
            }
        </style>
    </head>
    <body class="hotels_main">
        <!--<div class="se-pre-con"></div>-->

        <x-header />
        <div id="fullpage">
            @foreach($tt_contents as $content)
                @if($content->component_id == 2)
                    <x-secondaryPageVideoWithScrollerAndSlides :id="$content->id" />
                @elseif($content->component_id == 12)
                    <x-projectsGallery :id="$content->id" />
                @elseif($content->component_id == 4)
                    <x-footerComponent :id="$content->id" />
                @endif
            @endforeach
        </div>
        <script type="text/javascript" src="https://unpkg.com/aos@2.3.0/dist/aos.js"></script> 
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script> 
        <script src="js/bootstrap.min.js"></script> 
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.0/photoswipe.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.0/photoswipe-ui-default.min.js"></script> 
        <script src="js/jquery.magnific-popup.js"></script>  -->
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script>
            'use strict';
            /* global jQuery, PhotoSwipe, PhotoSwipeUI_Default, console */
            (function ($) {
                // Init empty gallery array
                var container = [];
                // Loop over gallery items and push it to the array
                $('#gallery').find('figure').each(function () {
                    var $link = $(this).find('a'),
                            item = {
                                src: $link.attr('href'),
                                w: $link.data('width'),
                                h: $link.data('height'),
                                title: $link.data('caption')
                            };
                    container.push(item);
                });
                // Define click event on gallery item
                $('#gallery a').click(function (event) {
                    // Prevent location change
                    event.preventDefault();
                    // Define object and gallery options
                    var $pswp = $('.pswp')[0],
                            options = {
                                index: $(this).parent('figure').index(),
                                bgOpacity: 1,
                                showHideOpacity: true
                            };
                    // Initialize PhotoSwipe
                    var gallery = new PhotoSwipe($pswp, PhotoSwipeUI_Default, container, options);
                    gallery.init();
                });

            }(jQuery));
        </script> 
        <script type="text/javascript" src="js/custom.js"></script> 
        <script>
            $(window).scroll(function () {
                if ($(this).scrollTop() > 300) {
                    $('.hotels_main').addClass('projects_main');
                } else {
                    $('.hotels_main').removeClass('projects_main');
                }
            });
            AOS.init({
                duration: 1000,
            })
            $(document).ready(function () {
                jQuery(document).ready(function ($) {
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 50) {
                            $('#backToTop').fadeIn('slow');
                        } else {
                            $('#backToTop').fadeOut('slow');
                        }
                    });
                    $('#backToTop').click(function () {
                        $("html, body").animate({scrollTop: 0}, 1500);
                        return false;
                    });
                });
                $('#myBtn').click(function () {
                    var bheight = $(window).height();
                    $("html, body").animate({scrollTop: bheight}, 600);
                    return false;
                });
                $('.go_up').click(function () {
                    $("html, body").animate({scrollTop: 0}, 900);
                    return false;
                });
            });
        </script> 
        <script>
            function myFunction() {
                var x = document.getElementById("myVideo").autoplay;
            }
            $(document).ready(function () {
                $('[data-fancybox]').fancybox({
                    infobar: false,
                    idleTime: false,
                    buttons: [
                        "close",
                    ],
                    tpl : {
                        closeBtn : '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>'
                    }
                });
                $.fancybox.defaults.idleTime = false;
            });
        </script> 
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-5D91DFSKVS"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'G-5D91DFSKVS');
        </script>
    </body>
</html>