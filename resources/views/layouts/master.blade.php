<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Styles -->
        @livewireStyles
        <!-- <link href="{{asset('css/fe-app.css')}}" rel="stylesheet">> -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.fullpage.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/viewScroller.css" />
        <link rel="stylesheet" type="text/css" href="fonts/fonts.css" />
        <link rel="stylesheet" type="text/css" href="https://unpkg.com/aos@2.3.0/dist/aos.css" />
        <style>
            #ascrail2000 {
                z-index: 9999 !important;
            }
            .slick-prev, .slick-next{
                border:none;
            }
            .slick-next::before, .slick-prev::before{

                    background-image: url(../images/right-arrow.svg);
            }
            .slick-prev:hover, .slick-prev:focus, .slick-next:hover, .slick-next:focus {
                background-color: transparent;
            }
        </style>
        <!-- Scripts -->
        <!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.js" defer></script> -->
         
        
    </head>
    <body class="hotels_main">
        {{ $slot }}
        @stack('modals')
        
        <!-- <script src="{{asset('js/fe-app.js')}}"></script> -->
        <!-- <script src="{{asset('js/tweeks.js')}}"></script> -->
        <script src="js/jquery-3.5.1.slim.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js"></script> 
        <script src="js/jquery.min.js"></script> 
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/jquery-ui.min.js"></script> 
        <script src="js/modernizr.js"></script> 
        <!-- <script src="js/fullpage.js"></script> -->
        <!-- <script type="text/javascript" src="js/scrolloverflow.js"></script>  -->
        <script type="text/javascript" src="js/jquery.fullpage.js"></script> 
        <script type="text/javascript" src="js/custom.js"></script>
        {{ $bodyscripts }}
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/jquery.mousewheel.min.js"></script>
        <script src="js/viewScroller.js"></script>
        <script type="text/javascript" src="https://unpkg.com/aos@2.3.0/dist/aos.js"></script>
    </body>
</html>
