require('./bootstrap');

require('alpinejs');
try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
    require('admin-lte');
    bsCustomFileInput = require('./../../node_modules/bs-custom-file-input/dist/bs-custom-file-input');
    bsCustomFileInput.init();
    require('./../../node_modules/admin-lte/plugins/summernote/summernote-bs4');
    require('./../../node_modules/admin-lte/plugins/select2/js/select2.full');
} catch (e) { }
