<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminPageController;
use App\Http\Controllers\Admin\AdminComponentController;
use App\Http\Controllers\Admin\AdminCarousalSlidesController;
use App\Http\Controllers\Admin\AdminExperienceController;
use App\Http\Controllers\Admin\AdminBrandsController;
use App\Http\Controllers\Admin\MediaArchiveCategoryController;
use App\Http\Controllers\Admin\MediaArchiveGalleryController;
use App\Http\Controllers\Admin\MediaArchiveController;
use App\Http\Controllers\Admin\ManifestoSlidesController;
use App\Http\Controllers\Admin\SocialMediaController;
use App\Http\Controllers\Admin\ProjectsGalleryController;
use App\Http\Controllers\Admin\ProjectImageController;
use App\Http\Controllers\Admin\ProjectSlotController;
use App\Http\Controllers\Pages\HomePageController;
use App\Http\Controllers\Pages\HotelsPageController;
use App\Http\Controllers\Pages\FnbPageController;
use App\Http\Controllers\Pages\SpaPageController;
use App\Http\Controllers\Pages\CollectionsPageController;
use App\Http\Controllers\Pages\ApproachPageController;
use App\Http\Controllers\Pages\ManifestoController;
use App\Http\Controllers\Pages\ProjectsController;
use App\Http\Controllers\Pages\ContactPageController;
use App\Http\Controllers\Pages\ArabicHomePageController;
use App\Http\Controllers\Pages\ArabicHotelsPageController;
use App\Http\Controllers\Pages\ArabicFnbPageController;
use App\Http\Controllers\Pages\ArabicSpaPageController;
use App\Http\Controllers\Pages\ArabicCollectionsPageController;
use App\Http\Controllers\Pages\ArabicApproachPageController;
use App\Http\Controllers\Pages\ArabicManifestoPageController;
use App\Http\Controllers\Pages\ArabicProjectsPageController;
use App\Http\Controllers\Pages\ArabicContactPageController;
use App\Http\Controllers\Auth\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomePageController::class, 'index']);
Route::get('/hotels', [HotelsPageController::class, 'index']);
Route::get('/fnb', [FnbPageController::class, 'index']);
Route::get('/spa', [SpaPageController::class, 'index']);
Route::get('/collections', [CollectionsPageController::class, 'index']);
Route::get('/approach', [ApproachPageController::class, 'index']);
Route::get('/manifesto', [ManifestoController::class, 'index']);
Route::get('/projects', [ProjectsController::class, 'index']);
Route::get('/contact', [ContactPageController::class, 'index']);
Route::get('/ar', [ArabicHomePageController::class, 'index']);
Route::get('/ar/hotels', [ArabicHotelsPageController::class, 'index']);
Route::get('/ar/fnb', [ArabicFnbPageController::class, 'index']);
Route::get('/ar/spa', [ArabicSpaPageController::class, 'index']);
Route::get('/ar/collections', [ArabicCollectionsPageController::class, 'index']);
Route::get('/ar/approach', [ArabicApproachPageController::class, 'index']);
Route::get('/ar/manifesto', [ArabicManifestoPageController::class, 'index']);
Route::get('/ar/projects', [ArabicProjectsPageController::class, 'index']);
Route::get('/ar/contact', [ArabicContactPageController::class, 'index']);
// Route::middleware(['auth:sanctum', 'verified'])->get('/user/register', [UserController::class, 'create']);
// Route::middleware(['auth:sanctum', 'verified'])->post('/user/register', [UserController::class, 'store'])->name('user.register');
//new

Route::get('/media/count', [ApproachPageController::class, 'viewCount'])->name('media.count');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('admin.dashboard');
})->name('dashboard');


Route::middleware(['auth:sanctum', 'verified'])->resource('admin/pages', AdminPageController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/component', AdminComponentController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/carousal', AdminCarousalSlidesController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/experience', AdminExperienceController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/brands', AdminBrandsController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/media-archive-category', MediaArchiveCategoryController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/media-archive/{id}/gallery', MediaArchiveGalleryController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/media-archive', MediaArchiveController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/manifesto-slide', ManifestoSlidesController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/social-media', SocialMediaController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/project', ProjectsGalleryController::class, ['as' => 'admin']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/project/{project_id}/project-image', ProjectImageController::class, ['as' => 'admin.project']);
Route::middleware(['auth:sanctum', 'verified'])->resource('admin/{tt_content_id}/project/{project_id}/slot-image', ProjectSlotController::class, ['as' => 'admin.project']);
Route::middleware(['auth:sanctum', 'verified'])->resource('users', UserController::class);
