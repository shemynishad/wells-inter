/*
 *
 *  Hotels Page Script Starts
 * 
 */
function myFunction() {
    var x = document.getElementById("myVideo").autoplay;
}

$(document).ready(function () {
    $('.slick-carousel').slick({
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 2000,
        content: true,
        dots: false,
        arrows: true,
        centerMode: true,
        responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,

                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                arrows: false,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
        ]
    });

    $(window).on('resize', function () {
        $('.slick-carousel').slick('resize');
    });

});

$(window).scroll(function () {
    if ($(this).scrollTop() > 300) {
        $('.hotels_main').addClass('projects_main');
    } else {
        $('.hotels_main').removeClass('projects_main');
    }
});

$(document).ready(function () {
    $('#myBtn').click(function () {
        var bheight = $(window).height();
        $("html, body").animate({scrollTop: bheight}, 600);
        return false;
    });
    $('#scroll_bottm').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });
});

$(document).ready(function() {
    document.getElementById("myVideo").controls = false;
    // Sets viewScroller
    $('.mainbag').viewScroller({
    useScrollbar: true,
    changeWhenAnim: false
    });
    AOS.init({
        duration: 1000,
    });
    $('[data-fancybox]').fancybox({
        toolbar  : false,
        smallBtn : true,
        iframe : {
            preload : false
        }
    });
});