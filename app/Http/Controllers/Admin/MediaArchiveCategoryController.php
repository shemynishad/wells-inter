<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MediaArchiveCategory;

class MediaArchiveCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tt_content_id)
    {
        $categories = MediaArchiveCategory::get();
        $categories->tt_content_id = $tt_content_id;
        return view('admin.media_archive_category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id)
    {
        return view('admin.media_archive_category.create', compact('tt_content_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form Validation
        $this->validate($request, [
            'category_name' => 'required|string',
        ]);
        $category = new MediaArchiveCategory;
        // $category->tt_content_id = $request->tt_content_id;
        $category->category_name = $request->category_name;
        $category->save();
        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Media Archive Category created successfully...');
        return redirect('admin/'.$request->tt_content_id.'/media-archive-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $id)
    {
        $category = MediaArchiveCategory::findOrFail($id);
        return view('admin.media_archive_category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $id)
    {
        $category = MediaArchiveCategory::findOrFail($id);
        $category->category_name = $request->category_name;
        $category->save();
        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Media Archive Category updated successfully...');
        return redirect('admin/'.$request->tt_content_id.'/media-archive-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $id)
    {
        $category = MediaArchiveCategory::findOrFail($id);
        $status = $category->delete();
        if($status) {
            \Session::flash('status', 'Success'); 
            \Session::flash('message', 'Media Archive Category deleted successfully...');
            return redirect('admin/'.$category->tt_content_id.'/media-archive-category');
        } else {
            \Session::flash('status', 'Error'); 
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/'.$category->tt_content_id.'/media-archive-category');
        }
    }
}
