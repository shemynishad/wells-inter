<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TtContent;

class AdminComponentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tt_content = TtContent::findOrFail($id);
        $tt_content_ar = TtContent::where('id', $tt_content->content_id_ar)->first();
        if ($tt_content_ar) {
            $tt_content['tt_content_id_ar'] = $tt_content_ar['id'];
            $tt_content['input_1_ar'] = $tt_content_ar['input_1'];
            $tt_content['input_2_ar'] = $tt_content_ar['input_2'];
            $tt_content['input_3_ar'] = $tt_content_ar['input_3'];
            $tt_content['input_4_ar'] =  $tt_content_ar['input_4'];
            $tt_content['input_5_ar'] =  $tt_content_ar['input_5'];
            $tt_content['input_6_ar'] =  $tt_content_ar['input_6'];
            $tt_content['text_1_ar'] =  $tt_content_ar['text_1'];
            $tt_content['text_2_ar'] =  $tt_content_ar['text_2'];
            $tt_content['text_3_ar'] =  $tt_content_ar['text_3'];
            $tt_content['text_4_ar'] =  $tt_content_ar['text_4'];
            $tt_content['headline_1_ar'] = $tt_content_ar['headline_1'];
            $tt_content['headline_2_ar'] =  $tt_content_ar['headline_2'];
            $tt_content['headline_3_ar'] =  $tt_content_ar['headline_3'];
            $tt_content['headline_4_ar'] =  $tt_content_ar['headline_4'];
            $tt_content['headline_5_ar'] =  $tt_content_ar['headline_5'];
            $tt_content['headline_6_ar'] =  $tt_content_ar['headline_6'];
        }
        return view('admin.components.component', compact('tt_content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tt_content = TtContent::findOrFail($id);
        if ($video_mp4 = $request->file('video_mp4')) {
            $name = $video_mp4->getClientOriginalName();
            $count = 1;
            while (file_exists('videos/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($video_mp4->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $video_mp4->move('videos/', $name);
            $tt_content->video_1 = $name;
        }
        if ($video_webm = $request->file('video_webm')) {
            $name = $video_webm->getClientOriginalName();
            $count = 1;
            while (file_exists('videos/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($video_webm->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $video_webm->move('videos/', $name);
            $tt_content->video_2 = $name;
        }
        if ($image_1 = $request->file('image_1')) {
            $name = $image_1->getClientOriginalName();
            $count = 1;
            while (file_exists('images/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image_1->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $image_1->move('images/', $name);
            $tt_content->image_1 = $name;
        }
        if ($image_2 = $request->file('image_2')) {
            $name = $image_2->getClientOriginalName();
            $count = 1;
            while (file_exists('images/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image_2->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $image_2->move('images/', $name);
            $tt_content->image_2 = $name;
        }
        if ($image_3 = $request->file('image_3')) {
            $name = $image_3->getClientOriginalName();
            $count = 1;
            while (file_exists('images/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image_3->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $image_3->move('images/', $name);
            $tt_content->image_3 = $name;
        }
        if ($image_4 = $request->file('image_4')) {
            $name = $image_4->getClientOriginalName();
            $count = 1;
            while (file_exists('images/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image_4->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $image_4->move('images/', $name);
            $tt_content->image_4 = $name;
        }
        if ($request->embed_video) {
            $tt_content->embed_video = $request->embed_video;
        }
        $tt_content->input_1 = $request->input_1;
        $tt_content->input_2 = $request->input_2;
        $tt_content->input_3 = $request->input_3;
        $tt_content->input_4 = $request->input_4;
        $tt_content->input_5 = $request->input_5;
        $tt_content->input_6 = $request->input_6;
        $tt_content->text_1 = $request->text_1;
        $tt_content->text_2 = $request->text_2;
        $tt_content->text_3 = $request->text_3;
        $tt_content->text_4 = $request->text_4;
        $tt_content->headline_1 = $request->headline_1;
        $tt_content->headline_2 = $request->headline_2;
        $tt_content->headline_3 = $request->headline_3;
        $tt_content->headline_4 = $request->headline_4;
        $tt_content->headline_5 = $request->headline_5;
        $tt_content->headline_6 = $request->headline_6;
        $tt_content->save();

        //for ar update
        if ($request->tt_content_id_ar) {
            $tt_content_ar = TtContent::findOrFail($request->tt_content_id_ar);
            if ($video_mp4 = $request->file('video_mp4')) {
                $name = $video_mp4->getClientOriginalName();
                $count = 1;
                while (file_exists('videos/' . $name)) {
                    $filename = pathinfo($name, PATHINFO_FILENAME);
                    $extension = pathinfo($name, PATHINFO_EXTENSION);
                    $name = pathinfo($video_mp4->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
                }
                $video_mp4->move('videos/', $name);
                $tt_content_ar->video_1 = $name;
            }
            if ($video_webm = $request->file('video_webm')) {
                $name = $video_webm->getClientOriginalName();
                $count = 1;
                while (file_exists('videos/' . $name)) {
                    $filename = pathinfo($name, PATHINFO_FILENAME);
                    $extension = pathinfo($name, PATHINFO_EXTENSION);
                    $name = pathinfo($video_webm->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
                }
                $video_webm->move('videos/', $name);
                $tt_content_ar->video_2 = $name;
            }
            if ($image_1 = $request->file('image_1')) {
                $name = $image_1->getClientOriginalName();
                $count = 1;
                while (file_exists('images/' . $name)) {
                    $filename = pathinfo($name, PATHINFO_FILENAME);
                    $extension = pathinfo($name, PATHINFO_EXTENSION);
                    $name = pathinfo($image_1->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
                }
                $image_1->move('images/', $name);
                $tt_content_ar->image_1 = $name;
            }
            if ($image_2 = $request->file('image_2')) {
                $name = $image_2->getClientOriginalName();
                $count = 1;
                while (file_exists('images/' . $name)) {
                    $filename = pathinfo($name, PATHINFO_FILENAME);
                    $extension = pathinfo($name, PATHINFO_EXTENSION);
                    $name = pathinfo($image_2->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
                }
                $image_2->move('images/', $name);
                $tt_content_ar->image_2 = $name;
            }
            if ($image_3 = $request->file('image_3')) {
                $name = $image_3->getClientOriginalName();
                $count = 1;
                while (file_exists('images/' . $name)) {
                    $filename = pathinfo($name, PATHINFO_FILENAME);
                    $extension = pathinfo($name, PATHINFO_EXTENSION);
                    $name = pathinfo($image_3->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
                }
                $image_3->move('images/', $name);
                $tt_content_ar->image_3 = $name;
            }
            if ($image_4 = $request->file('image_4')) {
                $name = $image_4->getClientOriginalName();
                $count = 1;
                while (file_exists('images/' . $name)) {
                    $filename = pathinfo($name, PATHINFO_FILENAME);
                    $extension = pathinfo($name, PATHINFO_EXTENSION);
                    $name = pathinfo($image_4->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
                }
                $image_4->move('images/', $name);
                $tt_content_ar->image_4 = $name;
            }
            if ($request->embed_video) {
                $tt_content_ar->embed_video = $request->embed_video;
            }
            $tt_content_ar->input_1 = $request->input_1_ar;
            $tt_content_ar->input_2 = $request->input_2_ar;
            $tt_content_ar->input_3 = $request->input_3_ar;
            $tt_content_ar->input_4 = $request->input_4_ar;
            $tt_content_ar->input_5 = $request->input_5_ar;
            $tt_content_ar->input_6 = $request->input_6_ar;
            $tt_content_ar->text_1 = $request->text_1_ar;
            $tt_content_ar->text_2 = $request->text_2_ar;
            $tt_content_ar->text_3 = $request->text_3_ar;
            $tt_content_ar->text_4 = $request->text_4_ar;
            $tt_content_ar->headline_1 = $request->headline_1_ar;
            $tt_content_ar->headline_2 = $request->headline_2_ar;
            $tt_content_ar->headline_3 = $request->headline_3_ar;
            $tt_content_ar->headline_4 = $request->headline_4_ar;
            $tt_content_ar->headline_5 = $request->headline_5_ar;
            $tt_content_ar->headline_6 = $request->headline_6_ar;
            $tt_content_ar->save();
        }

        \Session::flash('status', 'Success');
        \Session::flash('message', $tt_content->component->title . ' updated successfully...');
        return redirect('admin/pages/' . $tt_content->page_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
