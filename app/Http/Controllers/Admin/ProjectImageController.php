<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\Project;
use App\Models\ProjectImage;

class ProjectImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id, $project_id)
    {
        $project = Project::findOrFail($project_id);
        $project->tt_content_id = $tt_content_id;
        $project->project_id = $project_id;
        return view('admin.projects.images.create', compact('project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $tt_content_id, $project_id)
    {
        // Form Validation
        $this->validate($request, [
            'image' => 'required',
            'image_class' => 'required',
            'order' => 'required',
        ]);

        $project_image = new ProjectImage;
        if($image = $request->file('image')) {
            $name = $image->getClientOriginalName();
            $count = 1;
            while(file_exists('images/projects_gallery/'.$name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME).'('.$count++.').'.$extension;
            }
            $image->move('images/projects_gallery/',$name);
            $project_image->image = $name;
        }
        $project_image->image_caption = $request->image_caption;
        $project_image->image_class = $request->image_class;
        $project_image->order = $request->order;
        $project_image->project_id = $request->project_id;
        $project_image->save();

        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Project Gallery Image added successfully...');
        return redirect('admin/'.$tt_content_id.'/project');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $project_id, $id)
    {
        $project_image = ProjectImage::findOrFail($id);
        $project_image->tt_content_id = $tt_content_id;
        $project_image->project_id = $project_id;
        return view('admin.projects.images.edit', compact('project_image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $project_id, $id)
    {
        $project_image = ProjectImage::findOrFail($id);
        $oldImage = $project_image->image;
        if($image = $request->file('image')) {
            File::delete('images/projects_gallery/'.$oldImage);
            $name = $image->getClientOriginalName();
            $count = 1;
            while(file_exists('images/projects_gallery/'.$name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME).'('.$count++.').'.$extension;
            }
            $image->move('images/projects_gallery/',$name);
            $project_image->image = $name;
        }
        $project_image->image_caption = $request->image_caption;
        $project_image->image_class = $request->image_class;
        $project_image->order = $request->order;
        $project_image->save();

        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Project Gallery Image data updated successfully...');
        return redirect('admin/'.$tt_content_id.'/project');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $project, $id)
    {
        $project_image = ProjectImage::findOrFail($id);
        $status = $project_image->delete();
        File::delete('images/projects_gallery/'.$project_image->image);
        if($status) {
            \Session::flash('status', 'Success'); 
            \Session::flash('message', 'Project Image deleted successfully...');
            return redirect('admin/'.$tt_content_id.'/project');
        } else {
            \Session::flash('status', 'Error'); 
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/'.$tt_content_id.'/project');
        }
    }
}
