<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;

class AdminBrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tt_content_id)
    {
        $brands = Brand::where('tt_content_id', $tt_content_id)
            ->orderBy('order', 'ASC')
            ->get();
        $brands->tt_content_id = $tt_content_id;
        return view('admin.brands.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id)
    {
        return view('admin.brands.create', compact('tt_content_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form Validation
        $this->validate($request, [
            'brand_name' => 'required|string',
            'order' => 'required|integer',
            'brand_image' => 'required',
        ]);
        $brand = new Brand;
        $brand->tt_content_id = $request->tt_content_id;
        $brand->brand_name = $request->brand_name;
        $brand->order = $request->order;
        if ($brand_image = $request->file('brand_image')) {
            $name = $brand_image->getClientOriginalName();
            $count = 1;
            while (file_exists('images/brands/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($brand_image->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $brand_image->move('images/brands/', $name);
            $brand->brand_image = $name;
        }
        $brand->save();
        $insert_id = $brand->id;
        $brand = Brand::findOrFail($insert_id);
        if ($brand) {
            $brand_ar = new Brand;
            $brand_ar->tt_content_id = 44;
            $brand_ar->brand_name = $request->brand_name_ar ?? $request->brand_name;
            $brand_ar->order = $request->order;
            if ($brand_image = $request->file('brand_image')) {
                $brand_ar->brand_image = $brand->brand_image;
            }
            $brand_ar->save();
            $ar_insert_id = $brand_ar->id;

            $brand->brand_id_ar = $ar_insert_id;
            $brand->save();
        }
        \Session::flash('status', 'Success');
        \Session::flash('message', 'Brand slide created successfully...');
        return redirect('admin/' . $request->tt_content_id . '/brands');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $id)
    {
        $brand = Brand::findOrFail($id);
        if($brand->brand_id_ar){
            $brand_ar = Brand::findOrFail($brand->brand_id_ar); 
            $brand->brand_name_ar = $brand_ar->brand_name;
        }
        return view('admin.brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $id)
    {
        $brand = Brand::findOrFail($id);
        $brand->brand_name = $request->brand_name;
        
        $brand->order = $request->order;
        if ($brand_image = $request->file('brand_image')) {
            $name = $brand_image->getClientOriginalName();
            $count = 1;
            while (file_exists('images/brands/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($brand_image->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $brand_image->move('images/brands/', $name);
            $brand->brand_image = $name;
        }
        $brand->save();

        if ($brand->brand_id_ar) {
            $brand_ar = Brand::findOrFail($brand->brand_id_ar);
            $brand_ar->brand_name = $request->brand_name_ar ?? $request->brand_name;
            if ($brand_image = $request->file('brand_image')) {
                $brand_ar->brand_image = $name;
            }
            $brand_ar->save();
        }

        \Session::flash('status', 'Success');
        \Session::flash('message', 'Brand slide created successfully...');
        return redirect('admin/' . $request->tt_content_id . '/brands');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $id)
    {
        $brand = Brand::findOrFail($id);
        if ($brand->brand_id_ar) {
            $brand_ar = Brand::findOrFail($brand->brand_id_ar);
            $brand_ar->delete();
        }
        $status = $brand->delete();
        if ($status) {
            \Session::flash('status', 'Success');
            \Session::flash('message', 'Brand slide deleted successfully...');
            return redirect('admin/' . $brand->tt_content_id . '/brands');
        } else {
            \Session::flash('status', 'Error');
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/' . $brand->tt_content_id . '/brands');
        }
    }
}
