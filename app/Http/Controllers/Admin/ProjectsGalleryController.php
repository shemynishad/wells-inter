<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;

class ProjectsGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tt_content_id)
    {
        $projects = Project::where('tt_content_id', $tt_content_id)
                                ->orderBy('id', 'DESC')
                                ->get();
        $projects->tt_content_id = $tt_content_id;
        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id)
    {
        return view('admin.projects.create', compact('tt_content_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form Validation
        $this->validate($request, [
            'project_name' => 'required',
        ]);
        $project = new Project;
        $project->tt_content_id = $request->tt_content_id;
        $project->project_name = $request->project_name;
        $project->save();
        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'New Project created successfully...');
        return redirect('admin/'.$request->tt_content_id.'/project');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tt_content_id, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $id)
    {
        $project = Project::findOrFail($id);
        return view('admin.projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $id)
    {
        $project = Project::findOrFail($id);
        $project->project_name = $request->project_name;
        $project->save();
        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Project updated successfully...');
        return redirect('admin/'.$request->tt_content_id.'/project');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $id)
    {
        $project = Project::findOrFail($id);
        $status = $project->delete();
        if($status) {
            \Session::flash('status', 'Success'); 
            \Session::flash('message', 'Project deleted successfully...');
            return redirect('admin/'.$project->tt_content_id.'/project');
        } else {
            \Session::flash('status', 'Error'); 
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/'.$project->tt_content_id.'/project');
        }
    }
}
