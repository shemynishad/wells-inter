<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Experience;

class AdminExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tt_content_id)
    {
        $experiences = Experience::where('tt_content_id', $tt_content_id)
            ->get();
        $experiences->tt_content_id = $tt_content_id;
        return view('admin.experience.index', compact('experiences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id)
    {
        return view('admin.experience.create', compact('tt_content_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form Validation
        $this->validate($request, [
            'headline' => 'required',
            'sub_headline' => 'required',
        ]);
        $experience = new Experience;
        $experience->tt_content_id = $request->tt_content_id;
        $experience->headline = $request->headline;
        $experience->sub_headline = $request->sub_headline;
        $experience->save();
        $insert_id = $experience->id;
        $exp = Experience::findOrFail($insert_id);
        if ($exp) {
            $experience_ar = new Experience;
            $experience_ar->tt_content_id = 43;
            $experience_ar->headline = $request->headline_ar ?? $request->headline;
            $experience_ar->sub_headline = $request->sub_headline_ar ?? $request->sub_headline;
            $experience_ar->save();

            $ar_insert_id = $experience_ar->id;

            $exp->experience_id_ar = $ar_insert_id;
            $exp->save();
        }

        \Session::flash('status', 'Success');
        \Session::flash('message', 'Experience slide created successfully...');
        return redirect('admin/' . $request->tt_content_id . '/experience');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $tt_content_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $id)
    {
        $experience = Experience::findOrFail($id);
        $experience_ar = Experience::findOrFail($experience->experience_id_ar);
        if($experience_ar) {
        $experience->headline_ar = $experience_ar->headline;
        $experience->sub_headline_ar = $experience_ar->sub_headline;
        }
        return view('admin.experience.edit', compact('experience'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $tt_content_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $id)
    {
        $experience = Experience::findOrFail($id);
        $experience->headline = $request->headline;
        $experience->sub_headline = $request->sub_headline;
        $experience->save();

        if ($experience->experience_id_ar) {
            $exp_ar = Experience::findOrFail($experience->experience_id_ar);
            $exp_ar->headline = $request->headline_ar ?? $request->headline;
            $exp_ar->sub_headline = $request->sub_headline_ar ?? $request->sub_headline;
            $exp_ar->save();
        }

        \Session::flash('status', 'Success');
        \Session::flash('message', 'Experience slide created successfully...');
        return redirect('admin/' . $request->tt_content_id . '/experience');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $tt_content_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $id)
    {
        $experience = Experience::findOrFail($id);
        if ($experience->experience_id_ar) {
            $experience_ar = Experience::findOrFail($experience->experience_id_ar);
            $experience_ar->delete();
        }
        $status = $experience->delete();
        if ($status) {
            \Session::flash('status', 'Success');
            \Session::flash('message', 'Experience slide deleted successfully...');
            return redirect('admin/' . $experience->tt_content_id . '/experience');
        } else {
            \Session::flash('status', 'Error');
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/' . $experience->tt_content_id . '/experience');
        }
    }
}
