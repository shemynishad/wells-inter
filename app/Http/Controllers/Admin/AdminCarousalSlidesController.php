<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Carousal;

class AdminCarousalSlidesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tt_content_id)
    {
        $carousals = Carousal::where('tt_content_id', $tt_content_id)
                                ->orderBy('order', 'ASC')
                                ->get();
        $carousals->tt_content_id = $tt_content_id;
        return view('admin.carousal.index', compact('carousals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id)
    {
        return view('admin.carousal.create', compact('tt_content_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form Validation
        $this->validate($request, [
            'title' => 'required|string',
            'description' => 'required|string',
            'image_1' => 'required',
            'order' => 'required',
        ]);

        $carousal = new Carousal;
        if($image_1 = $request->file('image_1')) {
            $name = $image_1->getClientOriginalName();
            $count = 1;
            while(file_exists('images/carousal/'.$name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image_1->getClientOriginalName(), PATHINFO_FILENAME).'('.$count++.').'.$extension;
            }
            $image_1->move('images/carousal/',$name);
            $carousal->thumbnail = $name;
        }
        $carousal->title = $request->title;
        $carousal->title = $request->title_ar ?? $request->title;
        $carousal->order = $request->order;
        $carousal->description = $request->description;
        $carousal->description = $request->description_ar ?? $request->description;
        $carousal->tt_content_id = $request->tt_content_id;
        $carousal->save();
        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Carousal slide created successfully...');
        return redirect('admin/'.$request->tt_content_id.'/carousal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $tt_content_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $id)
    {
        $carousal = Carousal::findOrFail($id);
        return view('admin.carousal.edit', compact('carousal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $tt_content_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $id)
    {
        $carousal = Carousal::findOrFail($id);
        if($image_1 = $request->file('image_1')) {
            $name = $image_1->getClientOriginalName();
            $count = 1;
            while(file_exists('images/carousal/'.$name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image_1->getClientOriginalName(), PATHINFO_FILENAME).'('.$count++.').'.$extension;
            }
            $image_1->move('images/carousal/',$name);
            $carousal->thumbnail = $name;
        }
        $carousal->title = $request->title;
        $carousal->title_ar = $request->title_ar ?? $request->title;
        $carousal->order = $request->order;
        $carousal->description = $request->description;
        $carousal->description_ar = $request->description_ar ?? $request->description;
        $carousal->save();
        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Carousal slide updated successfully...');
        return redirect('admin/'.$carousal->tt_content_id.'/carousal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $tt_content_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $id)
    {
        $carousal = Carousal::findOrFail($id);
        $status = $carousal->delete();
        if($status) {
            \Session::flash('status', 'Success'); 
            \Session::flash('message', 'Carousal slide deleted successfully...');
            return redirect('admin/'.$carousal->tt_content_id.'/carousal');
        } else {
            \Session::flash('status', 'Error'); 
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/'.$carousal->tt_content_id.'/carousal');
        }
    }
}
