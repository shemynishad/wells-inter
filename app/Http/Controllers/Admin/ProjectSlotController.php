<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
// use Intervention\Image\Facades\Image as Image;
use App\Models\Project;
use App\Models\ProjectSlot;

class ProjectSlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id, $project_id)
    {
        $project = Project::findOrFail($project_id);
        $project->tt_content_id = $tt_content_id;
        $project->project_id = $project_id;
        return view('admin.projects.slots.create', compact('project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $tt_content_id, $project_id)
    {
        // Form Validation
        $this->validate($request, [
            'image' => 'required',
            'slot_id' => 'required',
        ]);

        $project_slot = new ProjectSlot;
        if($image = $request->file('image')) {
            $name = $image->getClientOriginalName();
            $count = 1;
            while(file_exists('images/projects_slots/'.$name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME).'('.$count++.').'.$extension;
            }
            $image->move('images/projects_slots/',$name);
            $project_slot->thumbnail = $name;
        }
        $project_slot->slot_id = $request->slot_id;
        $project_slot->project_id = $request->project_id;
        $project_slot->save();

        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Project Slot Image added successfully...');
        return redirect('admin/'.$tt_content_id.'/project');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $project_id, $id)
    {
        $project_slot = ProjectSlot::findOrFail($id);
        $project_slot->tt_content_id = $tt_content_id;
        $project_slot->project_id = $project_id;
        return view('admin.projects.slots.edit', compact('project_slot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $project_id, $id)
    {
        $project_slot = ProjectSlot::findOrFail($id);
        $oldThumbnail = $project_slot->thumbnail;
        if($image = $request->file('image')) {
            File::delete('images/projects_slots/'.$oldThumbnail);
            $name = $image->getClientOriginalName();
            $count = 1;
            while(file_exists('images/projects_slots/'.$name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME).'('.$count++.').'.$extension;
            }
            // $img = Image::make($image->path());
            // $img->fit(518, 699, function ($constraint) {
            //     $constraint->aspectRatio();
            //     $constraint->upSize();
            // })->save('images/projects_slots/'.$name);
            $image->move('images/projects_slots/',$name);
            $project_slot->thumbnail = $name;
        }
        $project_slot->slot_id = $request->slot_id;
        $project_slot->save();

        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Project Slot Image added successfully...');
        return redirect('admin/'.$tt_content_id.'/project');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $project, $id)
    {
        $project_slot = ProjectSlot::findOrFail($id);
        $status = $project_slot->delete();
        File::delete('images/projects_slots/'.$project_slot->thumbnail);
        if($status) {
            \Session::flash('status', 'Success'); 
            \Session::flash('message', 'Project Slot Image deleted successfully...');
            return redirect('admin/'.$tt_content_id.'/project');
        } else {
            \Session::flash('status', 'Error'); 
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/'.$tt_content_id.'/project');
        }
    }
}
