<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TtContent;
use App\Models\ManifestoSlide;

class ManifestoSlidesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tt_content_id)
    {
        $manifesto_slides = ManifestoSlide::where('tt_content_id', $tt_content_id)
            ->get();
        $manifesto_slides->tt_content_id = $tt_content_id;
        return view('admin.manifesto_slides.index', compact('manifesto_slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id)
    {
        return view('admin.manifesto_slides.create', compact('tt_content_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form Validation
        $this->validate($request, [
            'headline' => 'required|string',
            'sub_headline' => 'required|string',
            'quote' => 'required|string',
            'text' => 'required',
        ]);
        $manifesto_slide = new ManifestoSlide;
        $manifesto_slide->tt_content_id = $request->tt_content_id;
        $manifesto_slide->sub_headline = $request->sub_headline;
        $manifesto_slide->headline = $request->headline;
        $manifesto_slide->quote = $request->quote;
        $manifesto_slide->text = $request->text;
        $manifesto_slide->save();
        $insert_id = $manifesto_slide->id;
        $man_slide = ManifestoSlide::findOrFail($insert_id);
        if ($manifesto_slide) {
            $manifesto_slide_ar = new ManifestoSlide;
            $manifesto_slide_ar->tt_content_id = 51;
            $manifesto_slide_ar->sub_headline = $request->sub_headline_ar ?? $request->sub_headline;
            $manifesto_slide_ar->headline = $request->headline_ar ?? $request->headline;
            $manifesto_slide_ar->quote = $request->quote_ar ?? $request->quote;
            $manifesto_slide_ar->text = $request->text_ar ?? $request->text;
            $manifesto_slide_ar->save();
            $ar_insert_id = $manifesto_slide_ar->id;

            $man_slide->slide_id_ar = $ar_insert_id;
            $man_slide->save();
        }



        \Session::flash('status', 'Success');
        \Session::flash('message', 'Manifesto slide created successfully...');
        return redirect('admin/' . $request->tt_content_id . '/manifesto-slide');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $id)
    {
        $manifesto_slide = ManifestoSlide::findOrFail($id);
        $manifesto_slide->tt_content_id = $tt_content_id;

        $manifesto_slide_ar = ManifestoSlide::findOrFail($manifesto_slide->slide_id_ar);
        if ($manifesto_slide_ar) {
            $manifesto_slide->sub_headline_ar = $manifesto_slide_ar->sub_headline;
            $manifesto_slide->headline_ar = $manifesto_slide_ar->headline;
            $manifesto_slide->quote_ar = $manifesto_slide_ar->quote;
            $manifesto_slide->text_ar = $manifesto_slide_ar->text;
        }


        return view('admin.manifesto_slides.edit', compact('manifesto_slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $id)
    {
        $manifesto_slide = ManifestoSlide::findOrFail($id);
        $manifesto_slide->sub_headline = $request->sub_headline;
        $manifesto_slide->headline = $request->headline;
        $manifesto_slide->quote = $request->quote;
        $manifesto_slide->text = $request->text;
        $manifesto_slide->save();

        if ($manifesto_slide->slide_id_ar) {

            $manifesto_slide_ar = ManifestoSlide::findOrFail($manifesto_slide->slide_id_ar);
            $manifesto_slide_ar->sub_headline = $request->sub_headline_ar;
            $manifesto_slide_ar->headline = $request->headline_ar;
            $manifesto_slide_ar->quote = $request->quote_ar;
            $manifesto_slide_ar->text = $request->text_ar;
            $manifesto_slide_ar->save();
        }

        \Session::flash('status', 'Success');
        \Session::flash('message', 'Manifesto slide updated successfully...');
        return redirect('admin/' . $request->tt_content_id . '/manifesto-slide');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $id)
    {
        $manifesto_slide = ManifestoSlide::findOrFail($id);
        if ($manifesto_slide->slide_id_ar) {

            $manifesto_slide_ar = ManifestoSlide::findOrFail($manifesto_slide->slide_id_ar);
            $manifesto_slide_ar->delete();
        }
        $status = $manifesto_slide->delete();
        if ($status) {
            \Session::flash('status', 'Success');
            \Session::flash('message', 'Manifesto slide deleted successfully...');
            return redirect('admin/' . $manifesto_slide->tt_content_id . '/manifesto-slide');
        } else {
            \Session::flash('status', 'Error');
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/' . $manifesto_slide->tt_content_id . '/manifesto-slide');
        }
    }
}
