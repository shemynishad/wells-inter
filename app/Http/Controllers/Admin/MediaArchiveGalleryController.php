<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\MediaArchive;
use App\Models\MediaArchiveGallery;

class MediaArchiveGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id, $id)
    {
        $media_archive = new MediaArchive;
        $media_archive->tt_content_id = $tt_content_id;
        $media_archive->id = $id;
        return view('admin.media_archive_gallery.create', compact('media_archive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $tt_content_id, $id)
    {
        // Form Validation
        $this->validate($request, [
            'image' => 'required',
            'order' => 'required|integer',
        ]);
        $media_archive_gallery = new MediaArchiveGallery;
        if($image = $request->file('image')) {
            $name = $image->getClientOriginalName();
            $count = 1;
            while(file_exists('images/media_archive/'.$name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME).'('.$count++.').'.$extension;
            }
            $image->move('images/media_archive/',$name);
            $media_archive_gallery->image = $name;
        }
        $media_archive_gallery->media_archive_id = $request->media_archive_id;
        $media_archive_gallery->order = $request->order;
        $media_archive_gallery->save();

        $media_archive = MediaArchive::findOrFail($id);
        $media_archive->media_archive_gallery_id = $media_archive_gallery->id;
        $media_archive->save();

        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Meida Archive Gallery Image added successfully...');
        return redirect('admin/'.$request->tt_content_id.'/media-archive/'.$id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $media_archive_id, $id)
    {
        $media_archive_gallery = MediaArchiveGallery::findOrFail($id);
        $media_archive_gallery->tt_content_id = $tt_content_id;
        $media_archive_gallery->media_archive_id = $media_archive_id;
        return view('admin.media_archive_gallery.edit', compact('media_archive_gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $media_archive_id, $id)
    {
        $media_archive_gallery = MediaArchiveGallery::findOrFail($id);
        $oldImage = $media_archive_gallery->image;
        if($image = $request->file('image')) {
            $name = $image->getClientOriginalName();
            $count = 1;
            while(file_exists('images/media_archive/'.$name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME).'('.$count++.').'.$extension;
            }
            $image->move('images/media_archive/',$name);
            $media_archive_gallery->image = $name;
        }
        $media_archive_gallery->order = $request->order;
        $media_archive_gallery->save();
        File::delete('images/media_archive/'.$oldImage);
        \Session::flash('status', 'Success'); 
        \Session::flash('message', 'Meida Archive Gallery Image updated successfully...');
        return redirect('admin/'.$tt_content_id.'/media-archive/'.$media_archive_id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $media_archive_id, $id)
    {
        $media_archive_gallery = MediaArchiveGallery::findOrFail($id);
        $status = $media_archive_gallery->delete();
        File::delete('images/media_archive/'.$media_archive_gallery->image);
        if($status) {
            \Session::flash('status', 'Success'); 
            \Session::flash('message', 'Media Archive Gallery Image deleted successfully...');
            return redirect('admin/'.$tt_content_id.'/media-archive/'.$media_archive_id.'/edit');
        } else {
            \Session::flash('status', 'Error'); 
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/'.$tt_content_id.'/media-archive/'.$media_archive_id.'/edit');
        }
    }
}
