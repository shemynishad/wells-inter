<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MediaArchive;
use App\Models\MediaArchiveCategory;

class MediaArchiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tt_content_id)
    {
        $media_archives = MediaArchive::where('tt_content_id', $tt_content_id)
            ->orderBy('order', 'ASC')
            ->get();
        $media_archives->tt_content_id = $tt_content_id;
        return view('admin.media_archive.index', compact('media_archives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tt_content_id)
    {
        $media_archive = new MediaArchive;
        $categoryArray = array();
        $categories = MediaArchiveCategory::get();
        foreach ($categories as $category) {
            $categoryArray[$category->id] = $category->category_name;
        }
        $media_archive->tt_content_id = $tt_content_id;
        $media_archive->categories = $categoryArray;
        return view('admin.media_archive.create', compact('media_archive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form Validation
        $this->validate($request, [
            'media_archive_category_id' => 'required',
            'thumbnail' => 'required',
            'order' => 'required',
        ]);

        $media_archive = new MediaArchive;
        if ($thumbnail = $request->file('thumbnail')) {
            $thumb_name = $thumbnail->getClientOriginalName();
            $count = 1;
            while (file_exists('images/media_archive/' . $thumb_name)) {
                $filename = pathinfo($thumb_name, PATHINFO_FILENAME);
                $extension = pathinfo($thumb_name, PATHINFO_EXTENSION);
                $thumb_name = pathinfo($thumbnail->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $thumbnail->move('images/media_archive/', $thumb_name);
            $media_archive->thumbnail = $thumb_name;
        }
        if ($image = $request->file('image')) {
            $name = $image->getClientOriginalName();
            $count = 1;
            while (file_exists('images/media_archive/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $image->move('images/media_archive/', $name);
            $media_archive->image = $name;
        }
        $media_archive->video_url = $request->video_url;
        $media_archive->link = $request->link;
        $media_archive->media_archive_category_id = $request->media_archive_category_id;
        $media_archive->tt_content_id = $request->tt_content_id;
        $media_archive->media_archive_gallery_id = 0;
        $media_archive->order = $request->order;
        $media_archive->save();

        $insert_id = $media_archive->id;
        $MediaArch = MediaArchive::findOrFail($insert_id);
        if ($MediaArch) {
            $media_archive_ar = new MediaArchive;
            $media_archive_ar->tt_content_id = 46;
            if ($thumbnail = $request->file('thumbnail')) {
                $media_archive_ar->thumbnail = $thumb_name;
            }
            if ($image = $request->file('image')) {
                $media_archive->image = $name;
            }
            $media_archive_ar->video_url = $request->video_url;
            $media_archive_ar->link = $request->link;
            $media_archive_ar->media_archive_category_id = $request->media_archive_category_id;
            $media_archive_ar->media_archive_gallery_id = 0;
            $media_archive_ar->order = $request->order;
            $media_archive_ar->save();
            $ar_insert_id = $media_archive_ar->id;

            $MediaArch->media_id_ar = $ar_insert_id;
            $MediaArch->save();
        }


        \Session::flash('status', 'Success');
        \Session::flash('message', 'Meida Archive created successfully...');
        return redirect('admin/' . $request->tt_content_id . '/media-archive');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $tt_content_id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tt_content_id, $id)
    {
        $media_archive = MediaArchive::findOrFail($id);
        $categoryArray = array();
        $categories = MediaArchiveCategory::get();
        foreach ($categories as $category) {
            $categoryArray[$category->id] = $category->category_name;
        }
        $media_archive->tt_content_id = $tt_content_id;
        $media_archive->categories = $categoryArray;
        return view('admin.media_archive.edit', compact('media_archive'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tt_content_id, $id)
    {
        $media_archive = MediaArchive::findOrfail($id);
        if ($thumbnail = $request->file('thumbnail')) {
            $thumb_name = $thumbnail->getClientOriginalName();
            $count = 1;
            while (file_exists('images/media_archive/' . $thumb_name)) {
                $filename = pathinfo($thumb_name, PATHINFO_FILENAME);
                $extension = pathinfo($thumb_name, PATHINFO_EXTENSION);
                $thumb_name = pathinfo($thumbnail->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $thumbnail->move('images/media_archive/', $thumb_name);
            $media_archive->thumbnail = $thumb_name;
        }
        if ($image = $request->file('image')) {
            $name = $image->getClientOriginalName();
            $count = 1;
            while (file_exists('images/media_archive/' . $name)) {
                $filename = pathinfo($name, PATHINFO_FILENAME);
                $extension = pathinfo($name, PATHINFO_EXTENSION);
                $name = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME) . '(' . $count++ . ').' . $extension;
            }
            $image->move('images/media_archive/', $name);
            $media_archive->image = $name;
        }
        $media_archive->video_url = $request->video_url;
        $media_archive->link = $request->link;
        $media_archive->media_archive_category_id = $request->media_archive_category_id;
        $media_archive->order = $request->order;
        $media_archive->save();
        $media_id_ar = $media_archive->media_id_ar ?? "";
        if(!empty($media_id_ar)) {
            $media_archive_ar = MediaArchive::findOrFail($media_id_ar);
            if ($thumbnail = $request->file('thumbnail')) {
                $media_archive_ar->thumbnail = $thumb_name;
            }
            if ($image = $request->file('image')) {
                $media_archive_ar->image = $name;
            }
            $media_archive_ar->video_url = $request->video_url;
            $media_archive_ar->link = $request->link;
            $media_archive_ar->media_archive_category_id = $request->media_archive_category_id;
            $media_archive_ar->order = $request->order;
            $media_archive_ar->save();
        }
        \Session::flash('status', 'Success');
        \Session::flash('message', 'Meida Archive updated successfully...');
        return redirect('admin/' . $request->tt_content_id . '/media-archive');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tt_content_id, $id)
    {
        $media_archive = MediaArchive::findOrFail($id);
        $media_archive_ar = MediaArchive::findOrFail($media_archive->media_id_ar);
        if ($media_archive_ar) {
            $media_archive_ar->delete();
        }
        $status = $media_archive->delete();
        if ($status) {
            \Session::flash('status', 'Success');
            \Session::flash('message', 'Media Archive deleted successfully...');
            return redirect('admin/' . $media_archive->tt_content_id . '/media-archive');
        } else {
            \Session::flash('status', 'Error');
            \Session::flash('message', 'Some thing went wrong, Please try again later...');
            return redirect('admin/' . $media_archive->tt_content_id . '/media-archive');
        }
    }
}
