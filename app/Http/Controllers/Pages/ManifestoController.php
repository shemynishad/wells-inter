<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TtContent;

class ManifestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tt_contents = TtContent::where('page_id', 7)
                                    ->get();
        return view('pages.manifesto', compact('tt_contents'));
    }
}
