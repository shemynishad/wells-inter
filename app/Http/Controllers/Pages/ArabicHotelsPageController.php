<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TtContent;

class ArabicHotelsPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tt_contents = TtContent::where('page_id', 11)
                                    ->get();
        return view('pages.ar.hotels', compact('tt_contents'));
    }
}
