<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TtContent;

class SpaPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tt_contents = TtContent::where('page_id', 4)
                                    ->get();
        return view('pages.spa', compact('tt_contents'));
    }
}
