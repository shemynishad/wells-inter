<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TtContent;
use App\Models\MediaArchive;

class ApproachPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tt_contents = TtContent::where('page_id', 6)
            ->get();
        return view('pages.approach', compact('tt_contents'));
    }
    public function viewCount(Request $request)
    {
        $media_id = $request->media_id;
        $media_archive = MediaArchive::findOrfail($media_id);
        $media_archive->view_count = $media_archive->view_count + 1;
        $media_archive->save();
    }
}
