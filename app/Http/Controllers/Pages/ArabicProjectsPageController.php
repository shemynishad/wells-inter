<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TtContent;

class ArabicProjectsPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $tt_contents = TtContent::where('page_id', 17)
        //                             ->get();
        $tt_contents = TtContent::where('page_id', 8)
                                    ->get();
        return view('pages.ar.projects', compact('tt_contents'));
    }
}
