<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaArchive extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tt_content_id',
        'media_archive_category_id',
        'link',
        'thumbnail',
        'image',
        'media_archive_gallery_id',
    ];

    /*
     *  Relation ship with companies table
     * 
     * 
     */
    public function media_archive_category() {
        return $this->belongsTo('App\Models\MediaArchiveCategory');
    }

    /*
     *  Relation ship with companies table
     * 
     * 
     */
    public function media_archive_gallery() {
        return $this->hasMany('App\Models\MediaArchiveGallery')->orderBy('order', 'ASC');
    }
}
