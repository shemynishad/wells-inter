<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tt_content_id',
        'project_name',
    ];

    /*
     *  Relation ship with project_images table
     * 
     * 
     */
    public function project() {
        return $this->hasMany('App\Models\ProjectImage')->orderBy('order', 'ASC');
    }

    /*
     *  Relation ship with project_slots table
     * 
     * 
     */
    public function project_slot() {
        return $this->hasMany('App\Models\ProjectSlot')->orderBy('slot_id', 'ASC');
    }
}
