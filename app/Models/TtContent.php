<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TtContent extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'order', 'page_id', 'component_id',
        'input_1', 'input_2', 'input_3', 'input_4', 'input_5', 'input_6', 'input_7', 'input_8', 'input_9', 'input_10', 
        'headline_1', 'headline_2', 'headline_3', 'headline_4', 'headline_5', 'headline_6', 'headline_7', 'headline_8', 'headline_9', 'headline_10', 
        'text_1', 'text_2', 'text_3', 'text_4', 'text_5', 'text_6', 'text_7', 'text_8', 'text_9', 'text_10',
        'image_1', 'image_2', 'image_3', 'image_4', 'image_5', 'image_6', 'image_7', 'image_8', 'image_9', 'image_10',
        'link_1', 'link_2', 'link_3', 'link_4', 'link_5', 'link_6', 'link_7', 'link_8', 'link_9', 'link_10',
        'link_text_1', 'link_text_2', 'link_text_3', 'link_text_4', 'link_text_5', 'link_text_6', 'link_text_7', 'link_text_8', 'link_text_9', 'link_text_10',
        'video_1', 'video_2', 'video_3', 'video_4', 'video_5', 'video_6', 'video_7', 'video_8', 'video_9', 'video_10',
    ];

    /*
     *  Relation ship with components table
     * 
     * 
     */
    public function component() {
        return $this->belongsTo('App\Models\Component');
    }

    /*
     *  Relation ship with pages table
     * 
     * 
     */
    public function page() {
        return $this->belongsTo('App\Models\Page');
    }
}
