<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectSlot extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'slot_id',
        'thumbnail',
    ];

    /*
     *  Relation ship with projects table
     * 
     * 
     */
    public function project() {
        return $this->belongsTo('App\Models\Project');
    }
}
