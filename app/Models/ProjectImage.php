<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectImage extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id',
        'image',
        'image_class',
        'image_caption',
        'featured',
    ];

    /*
     *  Relation ship with projects table
     * 
     * 
     */
    public function project() {
        return $this->belongsTo('App\Models\Project');
    }
}
