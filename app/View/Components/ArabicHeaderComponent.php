<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\SocialMedia;

class ArabicHeaderComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.arabic-header-component');
    }

    /**
     *  Get The content
     * 
     */
    public function content() {
        $content = SocialMedia::find(1);
        return $content;
    }
}
