<x-master-admin-layout>
    <x-slot name="header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Dashboard') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin/component/{{ $media_archive->tt_content_id }}/edit">Media Archive</a></li>
                        <li class="breadcrumb-item active">Create Archive</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </x-slot>
    <main class="px-3">
        <section class="content">
            <!-- Default box -->
            @if(session()->has('message'))
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>{{ session()->get('status') }}!</strong> {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Media Archive Create</h3>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'post', 'route' => ['admin.media-archive.store', $media_archive->tt_content_id], 'class' => 'form-horizontal', 'files' => true]) !!}
                        {!! Form::hidden('tt_content_id', $media_archive->tt_content_id) !!}
                        <div class="form-group row">
                            {!! Form::label('inputCategory', 'Media Archive Category', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('media_archive_category_id', $media_archive->categories, null, ['class'=>'form-control','placeholder'=>'Select Media Archive Category', 'id'=>'inputCategory']) !!}
                                <span class="text-danger">{{ $errors->first('media_archive_category_id') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputCarousalOrder', 'Order', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('order', null , ['placeholder' => 'Order', 'class' => 'form-control', 'id' => 'inputCarousalOrder']) !!}
                                <span class="text-danger">{{ $errors->first('order') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputLink', 'Link', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('link', null , ['placeholder' => 'Link', 'class' => 'form-control', 'id' => 'inputLink']) !!}
                                <span class="text-danger">{{ $errors->first('link') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputThumbnail', 'Thumbnail', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                <div class="input-group col-sm-12">
                                    <div class="custom-file">
                                        {!! Form::file('thumbnail',['class' => 'custom-file-input', 'id' => 'inputThumbnail']) !!}
                                        {!! Form::label('inputThumbnail', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                    </div>
                                </div>
                                <span class="text-danger">{{ $errors->first('thumbnail') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('inputImage', 'Image', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                <div class="input-group col-sm-12">
                                    <div class="custom-file">
                                        {!! Form::file('image',['class' => 'custom-file-input', 'id' => 'inputImage']) !!}
                                        {!! Form::label('inputImage', 'Choose Image', ['class' => 'custom-file-label']) !!}
                                    </div>
                                </div>
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                {!! Form::submit('Submit', ['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            @endif
        </section>
    </main>
</x-master-admin-layout>
