<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\TtContent;
use App\Models\Brand;

class BrandsComponent extends Component
{
    public $id;
    /** 
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.brands-component');
    }

    /**
     *  Get The content
     * 
     */
    public function content($id) {
        $content = TtContent::find($id);
        $content->brands = Brand::where('tt_content_id', $id)
                                    ->orderBy('order', 'ASC')
                                    ->get();
        return $content;
    }
}
