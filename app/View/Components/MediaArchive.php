<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\TtContent;

class MediaArchive extends Component
{
    public $id;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.media-archive');
    }

    /**
     *  Get The content
     * 
     */
    public function content($id) {
        $content = TtContent::find($id);
        $content->media_archives = \App\Models\MediaArchive::where('tt_content_id', $id)
                                                    ->orderBy('order', 'ASC')
                                                    ->get();
        return $content;
    }
}
