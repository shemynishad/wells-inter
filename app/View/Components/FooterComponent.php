<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\TtContent;

class FooterComponent extends Component
{
    public $id;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.footer-component');
    }

    /**
     *  Get The content
     * 
     */
    public function content($id) {
        $content = TtContent::find($id);
        return $content;
    }
}
